"""
===========================================================
 Random forest clustering on a medical no show dataset
===========================================================
copy by Pierre Brossier
date : 17-05-2021

dataset from : https://www.kaggle.com/joniarroba/noshowappointments
code from    : https://towardsdatascience.com/machine-learning-with-datetime-feature-engineering-predicting-healthcare-appointment-no-shows-5e4ca3a85f96

In this example we'll try to compute cluster by random forest

=========== ========================================================
"""
print(__doc__)

# Re définition du chemin d'accès root
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
# ----------------------


# %%
# Load the 3rd parties libraries and created modules and import csv
# ----------------
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy import stats

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.metrics import roc_curve

from sklearn.model_selection import train_test_split

from EngieDataGenerator import getEngieDataSet


df = getEngieDataSet(50,2021,11)
print('names of columns are '+str(list(df.head(1))))

#verification nombre jour du mois
#df.iloc[0:364].groupby(['Month']).describe()


# %%s
# Show the dispersion in a subplots per data series types
# ----------------


#validation des données manquantes sur le dataset
#assert df........ .sum() == 0, 'missing ......'


zLim=max(df['Price'])

# affichage 3D du Prix-DayInWeek-DayInMonth
# set up a figure twice as wide as it is tall


fig,axs = plt.subplots(3,6,figsize=[2*6.4, 2*4.8])


axs[0,0]=plt.subplot(2,3,1, projection='3d')
axs[0,1]=plt.subplot(2,3,2, projection='3d')
axs[0,2]=plt.subplot(2,3,3, projection='3d')
axs[1,0]=plt.subplot(2,3,4, projection='3d')
axs[1,1]=plt.subplot(2,3,5, projection='3d')
axs[1,2]=plt.subplot(2,3,6, projection='3d')

#===============
#  First subplot
#===============
# set up the axes for the first plot
#axs[0] = fig.add_subplot(, 1, projection='3d')

# plot a 3D surface like in the example mplot3d/surface3d_demo
zdataA =df[df['Service'] ==0]['Price']
xdataA = df[df['Service'] ==0]['DayInWeek']
ydataA = df[df['Service'] ==0]['DayInMonth']

scatterA = axs[0,0].scatter3D(xdataA, ydataA, zdataA, c=(zdataA),cmap='viridis', linewidth=0.5)

axs[0,0].set_zlim(0, zLim)
axs[0,0].set_title('serie A')

#===============
# Second subplot
#===============
# set up the axes for the first plot
#ax = fig.add_subplot(3, 3, 2, projection='3d')
# set up the axes for the second plot
zdataB=df[df['Service'] ==1]['Price']
xdataB = df[df['Service'] ==1]['DayInWeek']
ydataB = df[df['Service'] ==1]['DayInMonth']
scatterB = axs[0,1].scatter3D(xdataB, ydataB, zdataB, c=(zdataB),cmap='plasma', linewidth=0.5)
axs[0,1].set_zlim(0, zLim)
axs[0,1].set_title('serie B')


#===============
#  Third subplot
#===============
# set up the axes for the first plot
#ax = fig.add_subplot(3, 3, 3, projection='3d')

# plot a 3D surface like in the example mplot3d/surface3d_demo
zdataC =df[df['Service'] ==2]['Price']
xdataC = df[df['Service'] ==2]['DayInWeek']
ydataC = df[df['Service'] ==2]['DayInMonth']
scatterC = axs[0,2].scatter3D(xdataC, ydataC, zdataC, c=(zdataC),cmap='magma', linewidth=0.5)
axs[0,2].set_zlim(0, zLim)
axs[0,2].set_title('serie C')
def calc_specificity(y_actual, y_pred, thresh):
 # calculates specificity
 return sum((y_pred < thresh) & (y_actual == 0)) /sum(y_actual ==0)


#===============
# Fourth subplot
#===============
# set up the axes for the first plot
#ax = fig.add_subplot(3, 3, 4, projection='3d')
# set up the axes for the second plot
zdataD=df[df['Service'] ==3]['Price']
xdataD = df[df['Service'] ==3]['DayInWeek']
ydataD = df[df['Service'] ==3]['DayInMonth']
scatterD = axs[1,0].scatter3D(xdataD, ydataD, zdataD, c=(zdataD),cmap='cividis', linewidth=0.5)
axs[1,0].set_zlim(0, zLim)
axs[1,0].set_title('serie D')


#===============
# Fiveth subplot
#===============
# set up the axes for the first plot
#ax = fig.add_subplot(3, 3, 5, projection='3d')
# set up the axes for the second plot
zdataE=df[df['Service'] ==4]['Price']
xdataE = df[df['Service'] ==4]['DayInWeek']
ydataE = df[df['Service'] ==4]['DayInMonth']
scatterE = axs[1,1].scatter3D(xdataE, ydataE, zdataE, c=(zdataE),cmap='summer', linewidth=0.5)
axs[1,1].set_zlim(0, zLim)
axs[1,1].set_title('serie E')

#===============
# Sixth subplot
#===============
# set up the axes for the first plot
#ax = fig.add_subplot(3, 3, 6, projection='3d')
# set up the axes for the second plot
zdata=df['Price']
xdata = df['DayInWeek']
ydata = df['DayInMonth']
scatter = axs[1,2].scatter3D(xdata, ydata, zdata, c=(zdata),cmap='gist_gray', linewidth=0.5)
axs[1,2].set_zlim(0, zLim)
axs[1,2].set_title('All series')

plt.show()

# %%s
# Transform the df matrix of days into batches of usecases : a year per service instance
# ----------------



# ----------------
def getDataSet(df):
 # creating headers
 firstInstance=pd.DataFrame(df.iloc[0:400])

 #day in week
 aWeek=firstInstance.groupby(by=['DayInWeek'])
 thisDayDescriptionDf=aWeek.Price.describe()
 # setting index for day
 a=list(range(len(thisDayDescriptionDf)))
 b=[str(a) for a in a]
 day_case_id=["D" + b for b in b]
 thisDayDescriptionDf.insert(0, "Id_case", day_case_id, True)
 thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="count", axis=1)
 thisDayDescriptionDf=thisDayDescriptionDf.reset_index()
 thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="DayInWeek", axis=1)

 #day in month
 aMonth=firstInstance.groupby(by=['DayInMonth'])
 #print(thisDf)
 thisMonthDescriptionDf=aMonth.Price.describe()
 thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="count", axis=1)
 thisMonthDescriptionDf=thisMonthDescriptionDf.reset_index()
 thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayInMonth", axis=1)

 # setting index for  months
 a=list(range(31))
 b=[str(a) for a in a]
 month_case_id=["M" + b for b in b]

 #print(thisMonthDescriptionDf)

 if thisMonthDescriptionDf['std'].isnull().values.any():
  thisMonthDescriptionDf['std']=thisMonthDescriptionDf['mean']

 #thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayInMonth", axis=1)

 for dayMissing in range(31-len(thisMonthDescriptionDf)):
  #
  idMissing=dayMissing+len(thisMonthDescriptionDf)
  meanDay=thisMonthDescriptionDf.mean()
  #print(np.transpose(meanDay))
  thisMonthDescriptionDf=thisMonthDescriptionDf.append(meanDay,ignore_index=True)
 #print(thisMonthDescriptionDf)
 thisMonthDescriptionDf.insert(0, "Id_case", month_case_id, True)

 #thisMonthDescriptionDf=thisMonthDescriptionDf.reset_index()
 #thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="index", axis=1)

 #print(thisDayDescriptionDf)
 #print(thisMonthDescriptionDf)
 #print(thisDayDescriptionDf.columns)
 #print(thisMonthDescriptionDf.columns)

 #concatenate
 thisDescriptionDf=pd.concat([thisDayDescriptionDf,thisMonthDescriptionDf], axis=0)
 thisDescriptionDf= thisDescriptionDf.set_index('Id_case')

 thisInstanceDf=thisDescriptionDf.stack(dropna=False).to_frame().T
 thisInstanceDf['Service']=firstInstance['Service']
 thisInstanceDf['ID_Instance']=firstInstance['ID_Instance']
 ds=pd.DataFrame(columns=thisInstanceDf.columns)


 #print(ds)
 for idx,instance in enumerate(df.groupby(by=['ID_Instance'])):
  #day in week
  thisDayDf=instance[1].groupby(by=['DayInWeek'])
  #print(thisDf)
  thisDayDescriptionDf=thisDayDf.Price.describe()
  thisDayDescriptionDf.insert(0, "Id_case", day_case_id, True)
  thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="count", axis=1)
  thisDayDescriptionDf=thisDayDescriptionDf.reset_index()
  thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="DayInWeek", axis=1)
  #thisDayDescriptionDf['Id_case']=list(range(len(thisDayDescriptionDf)))

  #day in month
  thisMonthDf=instance[1].groupby(by=['DayInMonth'])
  #print(len(thisMonthDf))
  thisMonthDescriptionDf=thisMonthDf.Price.describe()
  thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="count", axis=1)
  thisMonthDescriptionDf = thisMonthDescriptionDf.reset_index()
  thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayInMonth", axis=1)

  #si une seule valeur, pas de std donc on prend la moyenne
  if thisMonthDescriptionDf['std'].isnull().values.any():
   thisMonthDescriptionDf['std']=thisMonthDescriptionDf['mean']

  for dayMissing in range(31-len(thisMonthDescriptionDf)):

   idMissing=dayMissing+len(thisMonthDescriptionDf)
   meanDay=thisMonthDescriptionDf.mean()
   #print(np.transpose(meanDay))
   thisMonthDescriptionDf=thisMonthDescriptionDf.append(meanDay,ignore_index=True)

  thisMonthDescriptionDf.insert(0, "Id_case", month_case_id, True)





  #print(thisDayDescriptionDf.columns)
  #print(thisMonthDescriptionDf.columns)
  #concatenate
  thisDescriptionDf=pd.concat([thisDayDescriptionDf,thisMonthDescriptionDf], axis=0)
  thisDescriptionDf= thisDescriptionDf.set_index('Id_case')

  thisInstanceDf=thisDescriptionDf.stack(dropna=False).to_frame().T
  dayService=thisDayDf.Service.mean().reset_index().Service
  monthService=thisMonthDf.Service.mean().reset_index().Service
  serviceValue=np.mean(pd.concat([dayService,monthService], axis=0))
  thisInstanceDf['Service']=serviceValue

  dayID=thisDayDf.ID_Instance.mean().reset_index().ID_Instance
  monthID=thisMonthDf.ID_Instance.mean().reset_index().ID_Instance
  IDValue=np.mean(pd.concat([dayID,monthID], axis=0))
  thisInstanceDf['ID_Instance']=IDValue

  #print(thisInstanceDf.columns)
  #print(thisInstanceDf)
  ds=ds.append(thisInstanceDf,ignore_index=True)
 #print(ds)

 return ds
# ----------------

ds = getDataSet(df)






# %%s
# For each load cases show the signature
# ----------------
# affichage 3D du Prix-DayInWeek-DayInMonth
# set up a figure twice as wide as it is tall

def show_dataSet_signature(ds):

 zLim=max(ds.max(axis = 1, skipna = True))

 fig2,axs = plt.subplots(3,6,figsize=[2*6.4, 2*4.8])

 axs[0,0]=plt.subplot(2,3,1, projection='3d')
 axs[0,1]=plt.subplot(2,3,2, projection='3d')
 axs[0,2]=plt.subplot(2,3,3, projection='3d')
 axs[1,0]=plt.subplot(2,3,4, projection='3d')
 axs[1,1]=plt.subplot(2,3,5, projection='3d')
 axs[1,2]=plt.subplot(2,3,6, projection='3d')

 #===============
 #  First subplot
 #===============
 # set up the axes for the first plot

 #axs[0] = fig.add_subplot(, 1, projection='3d')

 # plot a 3D surface like in the example mplot3d/surface3d_demo

 dsA=ds[ds['Service'] ==0]
 if not dsA.empty:
  zdataA=np.empty([len(dsA),len(dsA.columns)-2])
  idy=0
  for idx,columns in enumerate(dsA):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataA[:,idy] =dsA[columns]
    idy=idy+1

  ny, nx = zdataA.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterA = axs[0,0].plot_surface(xv,yv,zdataA,rstride=1, cstride=1,
                  cmap='viridis', edgecolor='none')

  axs[0,0].set_xlabel("Signatures")
  axs[0,0].axes.xaxis.set_ticks([])
  axs[0,0].set_ylabel("jeu d'instance")
  axs[0,0].axes.yaxis.set_ticks([])
  axs[0,0].set_zlim(0, zLim)
  axs[0,0].set_title('serie A')

 #===============
 # Second subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 2, projection='3d')
 # set up the axes for the second plot
 dsB=ds[ds['Service'] ==1]
 if not dsB.empty:
  zdataB=np.empty([len(dsB),len(dsB.columns)-2])
  idy=0
  for idx,columns in enumerate(dsB):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataB[:,idy] =dsB[columns]
    idy=idy+1

  ny, nx = zdataB.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterB = axs[0,1].plot_surface(xv,yv,zdataB,rstride=1, cstride=1,
                  cmap='plasma', edgecolor='none')
  axs[0,1].set_zlim(0, zLim)
  axs[0,1].set_title('serie B')
  axs[0,1].set_xlabel("Signatures")
  axs[0,1].axes.xaxis.set_ticks([])
  axs[0,1].set_ylabel("jeu d'instance")
  axs[0,1].axes.yaxis.set_ticks([])
  axs[0,1].set_zlim(0, zLim)
  axs[0,1].set_title('serie B')

 #===============
 #  Third subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 3, projection='3d')

 # plot a 3D surface like in the example mplot3d/surface3d_demo
 dsC=ds[ds['Service'] ==2]
 if not dsC.empty:
  zdataC=np.empty([len(dsC),len(dsC.columns)-2])
  idy=0
  for idx,columns in enumerate(dsC):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataC[:,idy] =dsC[columns]
    idy=idy+1

  ny, nx = zdataC.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterC = axs[0,2].plot_surface(xv,yv,zdataC,rstride=1, cstride=1,
                  cmap='magma', edgecolor='none')
  axs[0,2].set_xlabel("Signatures")
  axs[0,2].axes.xaxis.set_ticks([])
  axs[0,2].set_ylabel("jeu d'instance")
  axs[0,2].axes.yaxis.set_ticks([])
  axs[0,2].set_zlim(0, zLim)
  axs[0,2].set_title('serie C')
 #===============
 # Fourth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 4, projection='3d')
 # set up the axes for the second plot
 dsD=ds[ds['Service'] ==3]
 if not dsD.empty:
  zdataD=np.empty([len(dsD),len(dsD.columns)-2])
  idy=0
  for idx,columns in enumerate(dsD):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataD[:,idy] =dsD[columns]
    idy=idy+1

  ny, nx = zdataD.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterD = axs[1,0].plot_surface(xv,yv,zdataD,rstride=1, cstride=1,
                  cmap='summer', edgecolor='none')
  axs[1,0].set_xlabel("Signatures")
  axs[1,0].axes.xaxis.set_ticks([])
  axs[1,0].set_ylabel("jeu d'instance")
  axs[1,0].axes.yaxis.set_ticks([])
  axs[1,0].set_zlim(0, zLim)
  axs[1,0].set_title('serie D')

 #===============
 # Fiveth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 5, projection='3d')
 # set up the axes for the second plot
 dsE=ds[ds['Service'] ==4]
 if not dsE.empty:
  zdataE=np.empty([len(dsE),len(dsE.columns)-2])
  idy=0
  for idx,columns in enumerate(dsE):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataE[:,idy] =dsE[columns]
    idy=idy+1

  ny, nx = zdataE.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterE = axs[1,1].plot_surface(xv,yv,zdataE,rstride=1, cstride=1,
                  cmap='viridis', edgecolor='none')
  axs[1,1].set_xlabel("Signatures")
  axs[1,1].axes.xaxis.set_ticks([])
  axs[1,1].set_ylabel("jeu d'instance")
  axs[1,1].axes.yaxis.set_ticks([])
  axs[1,1].set_zlim(0, zLim)
  axs[1,1].set_title('serie E')

 #===============
 # Sixth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 6, projection='3d')
 # set up the axes for the second plot
 zdata=np.empty([len(ds),len(ds.columns)-2])
 idy=0
 for idx,columns in enumerate(ds):
  #print(columns)
  if columns!=('Service','') and columns!=('ID_Instance',''):
   zdata[:,idy] =ds[columns]
   idy=idy+1

 ny, nx = zdata.shape
 x = range(nx)
 y = range(ny)
 xv, yv = np.meshgrid(x, y)

 scatter = axs[1,2].plot_surface(xv,yv,zdata,rstride=1, cstride=1,
                 cmap='gist_gray', edgecolor='none')
 axs[1,2].set_xlabel("Signatures")
 axs[1,2].axes.xaxis.set_ticks([])
 axs[1,2].set_ylabel("jeu d'instance")
 axs[1,2].axes.yaxis.set_ticks([])
 axs[1,2].set_zlim(0, zLim)
 axs[1,2].set_title('All series')


 fig2.show()

show_dataSet_signature(ds)

# %%s
# Treat the data and train the forrest
# ----------------
# affichage 3D du Prix-DayInWeek-DayInMonth
# set up a figure twice as wide as it is tall



# shuffle the samples
ds = ds.sample(n = len(ds))
ds_train, ds_test = train_test_split(ds, test_size=0.2)

'''
# select the right columns
col2use = ['ScheduledDay_day', 'ScheduledDay_hour',
 'ScheduledDay_minute', 'ScheduledDay_dayofweek',
 'AppointmentDay_day',
 'AppointmentDay_dayofweek', 'delta_days']
'''
# ----------------
def get_X_y(ds):

 # creation des columns
 y = ds['Service'].values

 ds = ds.drop(labels="Service", axis=1)
 ds = ds.drop(labels="ID_Instance", axis=1)
 X = ds.values

 return X , y
# ----------------

X_train, y_train =get_X_y(ds_train)
X_test, y_test =get_X_y(ds_test)

# %%
# Training of the random forest classifier from sklearn
# ----------------

# creation du ML et entrainement
rf=RandomForestClassifier(bootstrap=True, random_state = 1)

#rf=RandomForestClassifier(bootstrap=True, max_depth = 15, n_estimators=100, random_state = 1,n_classes_int=list(range(5)))


rf.fit(X_train, y_train)

# %%
# Access results
# ----------------
def generate_report(rf,x_iput,y_output,stringName):

 y_result = rf.predict(x_iput)

 score = rf.score(x_iput,y_output)

 proba = rf.predict_proba(x_iput)

 worst=np.zeros_like(y_output)
 best=np.zeros_like(y_output)

 for idx,row in enumerate(proba):
   best[idx]=row[int(y_output[idx])]
   row[int(y_output[idx])]=0
   worst[idx]=max(row)
# ----------------

 #edit rapport
 print('Random Forest')
 print('----------------------')
 print('\n')
 print('Reussite '+stringName+' : '+str(score*100)+'%')
 print('\n')
 worstString='\n'

 worstDF=pd.DataFrame(worst)
 print('Mean Worst Guess proba '+stringName+' : '+str(round(worstDF.describe().loc[ 'mean' , : ][0]*100,3))+'%')

 bestDF=pd.DataFrame(best)
 print('Mean Best Guess proba '+stringName+' : '+str(round(bestDF.describe().loc[ 'mean' , : ][0]*100,3))+'%')


 '''
 for r in worst:
  worstString=worstString+str(r)+'\n'
 print('Worst '+stringName+':'+worstString)

 bestString='\n'
 for r in best:
  bestString=bestString+str(r)+'\n'
 print('Best '+stringName+':'+bestString)
 '''


generate_report(rf,X_train,y_train,'Training')
generate_report(rf,X_test,y_test,'Testing')

# %%
# generating another test same calendar
# ----------------


small_df = getEngieDataSet(10,2021,11)
small_ds=getDataSet(small_df)
X_small, y_small =get_X_y(small_ds)

#proba = rf.predict_proba(X_small)
#print(proba)
#print(y_small)
#print(rf.predict(X_small))

generate_report(rf,X_small, y_small,'même année')




# %%
# generating another test another calendar
# ----------------


small_df = getEngieDataSet(10,2020,6)
small_ds=getDataSet(small_df)
X_small, y_small =get_X_y(small_ds)

#proba = rf.predict_proba(X_small)
#print(proba)
#print(y_small)
#print(rf.predict(X_small))

generate_report(rf,X_small, y_small,'autre année')


# %%
# generating monthly test
# ----------------
small_df = getEngieDataSet(1,2021,11)

df_month=list()
for iMonth in range(12):
 df_month.append(small_df.loc[(small_df['Month']==iMonth+1)])

ds_month=list()
for dfMonth in df_month:
 ds_month.append(getDataSet(dfMonth))


for idMonth,dsMonth in enumerate(ds_month):
 X_small, y_small =get_X_y(dsMonth)
 generate_report(rf,X_small, y_small,'month n°'+str(idMonth))

proba = rf.predict_proba(X_small)
#print(proba)
#print(y_small)
#print(rf.predict(X_small))

# %%
# generating a noise test on Price
# ----------------


small_df = getEngieDataSet(2,2021,11)

mu, sigma = 0, 0.1
# creating a noise with the same dimension as the dataset (2,2)
noise_Price = np.random.normal(mu, sigma, small_df['Price'].shape)

noised_df = copy.copy(small_df)

noised_df['Price'] = small_df['Price']*noise_Price

small_ds=getDataSet(small_df)
noised_ds=getDataSet(noised_df)
show_dataSet_signature(small_ds)

df_month=list()
for iMonth in range(12):
 df_month.append(noised_df.loc[(noised_df['Month']==iMonth+1)])

ds_month=list()
for dfMonth in df_month:
 ds_month.append(getDataSet(dfMonth))


for idMonth,dsMonth in enumerate(ds_month):
 X_small, y_small =get_X_y(dsMonth)
 #generate_report(rf,X_small, y_small,'month n°'+str(idMonth))
print(y_small)
print(rf.predict(X_small))