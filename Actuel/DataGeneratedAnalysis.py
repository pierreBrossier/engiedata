"""
===========================================================
 Random forest clustering on a medical no show dataset
===========================================================
copy by Pierre Brossier
date : 17-05-2021

dataset from : https://www.kaggle.com/joniarroba/noshowappointments
code from    : https://towardsdatascience.com/machine-learning-with-datetime-feature-engineering-predicting-healthcare-appointment-no-shows-5e4ca3a85f96

In this example we'll try to compute cluster by random forest

=========== ========================================================
"""
print(__doc__)

# Re définition du chemin d'accès root
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
# ----------------------


# %%
# Load the 3rd parties libraries and created modules and import csv
# ----------------
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy import stats

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.metrics import roc_curve

from sklearn.model_selection import train_test_split

from EngieDataGenerator import getEngieDataSet

from EngieDataCollection import getDataSet
from EngieDataCollection import get_X_y

from showDataCollection import show_dataSet_rawCollection
from showDataCollection import show_dataSet_signature
from showDataCollection import generate_report

# %%s
# Generate data
# ----------------

df = getEngieDataSet(50,2021,11)
print('names of columns are '+str(list(df.head(1))))

# %%s
# Show data
# ----------------

show_dataSet_rawCollection(df)

# %%s
# Consolidate Data
# ----------------


ds = getDataSet(df)

# %%s
# For each load cases show the signature
# ----------------

show_dataSet_signature(ds)

# %%s
# Treat the data and train the forrest
# ----------------
# affichage 3D du Prix-DayInWeek-DayInMonth
# set up a figure twice as wide as it is tall


# shuffle the samples
ds = ds.sample(n = len(ds))
ds_train, ds_test = train_test_split(ds, test_size=0.2)

# Separate train from test
X_train, y_train =get_X_y(ds_train)
X_test, y_test =get_X_y(ds_test)

# %%
# Training of the random forest classifier from sklearn
# ----------------

# creation du ML et entrainement
rf=RandomForestClassifier(bootstrap=True, random_state = 1)

rf.fit(X_train, y_train)

# %%
# Access results
# ----------------

generate_report(rf,X_train,y_train,'Training')
generate_report(rf,X_test,y_test,'Testing')

# %%
# generating another test same calendar
# ----------------


small_df = getEngieDataSet(10,2021,11)
small_ds=getDataSet(small_df)
X_small, y_small =get_X_y(small_ds)


generate_report(rf,X_small, y_small,'même année')




# %%
# generating another test another calendar
# ----------------


small_df = getEngieDataSet(10,2020,6)
small_ds=getDataSet(small_df)
X_small, y_small =get_X_y(small_ds)


generate_report(rf,X_small, y_small,'autre année')


# %%
# generating monthly test
# ----------------
small_df = getEngieDataSet(1,2021,11)

df_month=list()
for iMonth in range(12):
 df_month.append(small_df.loc[(small_df['Month']==iMonth+1)])

ds_month=list()
for dfMonth in df_month:
 ds_month.append(getDataSet(dfMonth))


for idMonth,dsMonth in enumerate(ds_month):
 X_small, y_small =get_X_y(dsMonth)
 generate_report(rf,X_small, y_small,'month n°'+str(idMonth))

# %%
# generating a noise test on Price
# ----------------


small_df = getEngieDataSet(2,2021,11)

mu, sigma = 0, 0.1
# creating a noise with the same dimension as the dataset (2,2)
noise_Price = np.random.normal(mu, sigma, small_df['Price'].shape)

noised_df = copy.copy(small_df)

noised_df['Price'] = small_df['Price']*noise_Price

small_ds=getDataSet(small_df)
noised_ds=getDataSet(noised_df)
show_dataSet_signature(small_ds)

df_month=list()
for iMonth in range(12):
 df_month.append(noised_df.loc[(noised_df['Month']==iMonth+1)])

ds_month=list()
for dfMonth in df_month:
 ds_month.append(getDataSet(dfMonth))


for idMonth,dsMonth in enumerate(ds_month):
 X_small, y_small =get_X_y(dsMonth)
 #generate_report(rf,X_small, y_small,'month n°'+str(idMonth))
print(y_small)
print(rf.predict(X_small))