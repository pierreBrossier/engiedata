"""
===========================================================
 Random forest clustering on a medical no show dataset
===========================================================
copy by Pierre Brossier
date : 17-05-2021

dataset from : https://www.kaggle.com/joniarroba/noshowappointments
code from    : https://towardsdatascience.com/machine-learning-with-datetime-feature-engineering-predicting-healthcare-appointment-no-shows-5e4ca3a85f96

In this example we'll try to compute cluster by random forest

=========== ========================================================
"""
print(__doc__)

# Re définition du chemin d'accès root
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
# ----------------------


# %%
# Load the 3rd parties libraries and created modules and import csv
# ----------------

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.metrics import roc_curve

from sklearn.model_selection import train_test_split

df = pd.read_csv('data/KaggleV2-May-2016.csv')
print('names of columns are '+str(list(df.head(1))))


def calc_specificity(y_actual, y_pred, thresh):
 # calculates specificity
 return sum((y_pred < thresh) & (y_actual == 0)) /sum(y_actual ==0)

def print_report(y_actual, y_pred, thresh):

 auc = roc_auc_score(y_actual, y_pred)
 accuracy = accuracy_score(y_actual, (y_pred > thresh))
 recall = recall_score(y_actual, (y_pred > thresh))
 precision = precision_score(y_actual, (y_pred > thresh))
 specificity = calc_specificity(y_actual, y_pred, thresh)
 print('AUC:%.3f'%auc)
 print('accuracy:%.3f'%accuracy)
 print('recall:%.3f'%recall)
 print('precision:%.3f'%precision)
 print('specificity:%.3f'%specificity)
 print('prevalence:%.3f'%calc_prevalence(y_actual))
 print(' ')
 return auc, accuracy, recall, precision, specificity

def calc_prevalence(y):
 return (sum(y)/len(y))

# %%s
# Load the dataset, consolidate it, split the train and check data
# ----------------

df['OUTPUT_LABEL'] = (df['No-show'] == 'Yes').astype('int')

df['ScheduledDay'] = pd.to_datetime(df['ScheduledDay'],
 format = '%Y-%m-%dT%H:%M:%SZ',
 errors = 'coerce')
df['AppointmentDay'] = pd.to_datetime(df['AppointmentDay'],
 format = '%Y-%m-%dT%H:%M:%SZ',
 errors = 'coerce')

#vaisation des données manquantes sur le dataset
assert df.ScheduledDay.isnull().sum() == 0, 'missing ScheduledDay dates'
assert df.AppointmentDay.isnull().sum() == 0, 'missing AppointmentDay dates'

#petit ajustrement sur le dataset
df['AppointmentDay'] = df['AppointmentDay'] +pd.Timedelta('1d') - pd.Timedelta('1s')
df=df.loc[(df['ScheduledDay']<=df['AppointmentDay'])].copy()

# from dataTime to integerin tables
df['ScheduledDay_year'] = df['ScheduledDay'].dt.year
df['ScheduledDay_month'] = df['ScheduledDay'].dt.month
df['ScheduledDay_week'] = df['ScheduledDay'].dt.week
df['ScheduledDay_day'] = df['ScheduledDay'].dt.day
df['ScheduledDay_hour'] = df['ScheduledDay'].dt.hour
df['ScheduledDay_minute'] = df['ScheduledDay'].dt.minute
df['ScheduledDay_dayofweek'] = df['ScheduledDay'].dt.dayofweek
df['AppointmentDay_year'] = df['AppointmentDay'].dt.year
df['AppointmentDay_month'] = df['AppointmentDay'].dt.month
df['AppointmentDay_week'] = df['AppointmentDay'].dt.week
df['AppointmentDay_day'] = df['AppointmentDay'].dt.day
df['AppointmentDay_hour'] = df['AppointmentDay'].dt.hour
df['AppointmentDay_minute'] = df['AppointmentDay'].dt.minute
df['AppointmentDay_dayofweek'] = df['AppointmentDay'].dt.dayofweek

#rajout d'une colonne comptant le temps entre la date de reservation et le jour du rendez-vous
df['delta_days'] = (df['AppointmentDay']-df['ScheduledDay']).dt.total_seconds()/(60*60*24)

# affichage d'un historigramme
plt.hist(df.loc[df.OUTPUT_LABEL == 1,'delta_days'],
 label = 'Missed',bins = range(0,60,1))
plt.hist(df.loc[df.OUTPUT_LABEL == 0,'delta_days'],
 label = 'Not Missed',bins = range(0,60,1),alpha =0.5)
plt.legend()
plt.xlabel('days until appointment')
plt.ylabel('normed distribution')
plt.xlim(0,40)
plt.show()






# shuffle the data frame
df = df.sample(n = len(df), random_state = 42)
df = df.reset_index(drop = True)
df_valid = df.sample(frac = 0.3, random_state = 42)
df_train = df.drop(df_valid.index)

# select the right columns
col2use = ['ScheduledDay_day', 'ScheduledDay_hour',
 'ScheduledDay_minute', 'ScheduledDay_dayofweek',
 'AppointmentDay_day',
 'AppointmentDay_dayofweek', 'delta_days']

# creation des columns
X_train = df_train[col2use].values
X_valid = df_valid[col2use].values
y_train = df_train['OUTPUT_LABEL'].values
y_valid = df_valid['OUTPUT_LABEL'].values
print('Training shapes:',X_train.shape, y_train.shape)
print('Validation shapes:',X_valid.shape, y_valid.shape)

# %%
# Training of the random forest classifier from sklearn
# ----------------

# creation du ML et entrainement
rf=RandomForestClassifier(max_depth = 5, n_estimators=100, random_state = 42)
rf.fit(X_train, y_train)

# %%
# access predictions
y_train_preds = rf.predict_proba(X_train)[:,1]
y_valid_preds = rf.predict_proba(X_valid)[:,1]

#edit rapport
thresh = 0.201
print('Random Forest')
print('Training :')
print_report(y_train,y_train_preds,thresh)
print('Validation:')
print_report(y_valid,y_valid_preds,thresh)

# print ROC curve and yield Area Under Curve AUC (best 1, random 0.5)
fpr_train, tpr_train, thresholds_train = roc_curve(y_train, y_train_preds)
auc_train = roc_auc_score(y_train, y_train_preds)
fpr_valid, tpr_valid, thresholds_valid = roc_curve(y_valid, y_valid_preds)
auc_valid = roc_auc_score(y_valid, y_valid_preds)
plt.plot(fpr_train, tpr_train, 'r-',label ='Train AUC:%.3f'%auc_train)
plt.plot(fpr_valid, tpr_valid, 'b-',label ='Valid AUC:%.3f'%auc_valid)
plt.plot([0,1],[0,1],'k-')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.legend()
plt.show()

# get the feature importance for top num and sort in reverse order
feature_importances = pd.DataFrame(rf.feature_importances_,
 index = col2use,
 columns=['importance']).sort_values('importance',
 ascending=False)
num = min([50,len(col2use)])
ylocs = np.arange(num)
values_to_plot = feature_importances.iloc[:num].values.ravel()[::-1]
feature_labels = list(feature_importances.iloc[:num].index)[::-1]
plt.figure(num=None, figsize=(6, 6), dpi=80, facecolor='w', edgecolor='k')
plt.barh(ylocs, values_to_plot, align = 'center')
plt.ylabel('Features')
plt.xlabel('Importance Score')
plt.title('Feature Importance Score — Random Forest')
plt.yticks(ylocs, feature_labels)
plt.show()
