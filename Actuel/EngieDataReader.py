"""
===========================================================
 Reading engie calendar-cost dataset
===========================================================
Pierre Brossier
date : 27-05-2021

This fonction reads and return the calendar-cost dataset.
A single panda dataframe is returned


=========== ========================================================
Access to files :
"""
print(__doc__)

# ----------------------
# Third-party libraries
# ----------------------
import numpy as np
import pandas as pd
import math
import os
import sys
import copy
import csv
import random

import calendar
import time

import hashlib

import matplotlib.pyplot as plt
# Import seaborn
import seaborn as sns

from dataclasses import dataclass, field
from typing import Callable, ClassVar, Dict, Optional


# ----------------------
# Re définition du chemin d'accès root
# ----------------------
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
print('\n')

# ----------------------
# Import Jour ferié
# ----------------------
workHolliday_df = pd.read_csv('jours_feries_metropole.csv',encoding='utf-8')
# ----------------------

# ----------------------
# Import Data Engie
# ----------------------

#trois mois
#RawCalendarCost_df = pd.read_csv('2021-05-26 aws-cost-and-usage-deaily.csv',encoding='utf-16',sep=';',header=0)

# l'année
tic=time.perf_counter()

RawCalendarCost_df = pd.read_csv('2021-06-03 aws-cost-and-usage.csv',encoding='utf-16',sep=';',header=0)



# %%
# Creation de fonction et classe utile
# ----------------
class TimerError(Exception):
    """A custom exception used to report errors in use of Timer class"""

@dataclass
class Timer:
    timers: ClassVar[Dict[str, float]] = dict()
    name: Optional[str] = None
    text: str = "Elapsed time: {:0.4f} seconds"
    logger: Optional[Callable[[str], None]] = print
    _start_time: Optional[float] = field(default=None, init=False, repr=False)

    def __post_init__(self) -> None:
        """Add timer to dict of timers after initialization"""
        if self.name is not None:
            self.timers.setdefault(self.name, 0)

    def start(self) -> None:
        """Start a new timer"""
        if self._start_time is not None:
            raise TimerError(f"Timer is running. Use .stop() to stop it")

        self._start_time = time.perf_counter()

    def stop(self) -> float:
        """Stop the timer, and report the elapsed time"""
        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")

        # Calculate elapsed time
        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None

        # Report elapsed time
        if self.logger:
            self.logger(self.text.format(elapsed_time))
        if self.name:
            self.timers[self.name] += elapsed_time

        return elapsed_time


def string_comparison(stringA,stringB):

        while True:
                try:
                        if len(stringA)==len(stringB):
                                boulean= (stringA==stringB)
                        else:
                                boulean=False
                        break
                except ValueError:
                        print("Oops!  That was no valid number.  Try again...")
        return boulean


# %%
# Consolidation des data
# ----------------


# ----------------------
# Creation d'une table d'equivalence
# ----------------------
#columns=['IDInstance',list(RawCalendarCost_df.columns[3:5])]


test =RawCalendarCost_df.head(500)


tic=time.perf_counter()
# on enlève les colonnes inutiles
TidyCalendarCost_df=RawCalendarCost_df
TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['CSP', 'Period End'])
# on change les data rame en table
TidyCalendarCost_df['Year']=pd.to_datetime(TidyCalendarCost_df['Period Start']).dt.year
TidyCalendarCost_df['Month']=pd.to_datetime(TidyCalendarCost_df['Period Start']).dt.month
TidyCalendarCost_df['DayInYear']=pd.to_datetime(TidyCalendarCost_df['Period Start']).dt.dayofyear
TidyCalendarCost_df['DayOfMonth']=pd.to_datetime(TidyCalendarCost_df['Period Start']).dt.day
TidyCalendarCost_df['DayOfWeek']=pd.to_datetime(TidyCalendarCost_df['Period Start']).dt.dayofweek


sMonth=pd.to_datetime(TidyCalendarCost_df['Period Start']).dt.month_name()

TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['Period Start'])

#TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['Period Start'])
TidyCalendarCost_df.insert (0, 'ID_Instance', TidyCalendarCost_df['Year']*0)
TidyCalendarCost_df.insert (1, 'ID_Account', TidyCalendarCost_df['Year']*0)
TidyCalendarCost_df.insert (2, 'ID_Service', TidyCalendarCost_df['Year']*0)

TidyCalendarCost_df.insert (1, 'hash_Instance', TidyCalendarCost_df['Account']+TidyCalendarCost_df['Service'])

nInstances=len(TidyCalendarCost_df.groupby(['hash_Instance']).count())


print('il y a '+str(nInstances)+' instances différentes')
print('   dont '+str(len(TidyCalendarCost_df.groupby(['Account']).count()))+' accounts différents')
print('    et  '+str(len(TidyCalendarCost_df.groupby(['Service']).count()))+' services différents')

sum1=0
sum2=0
sum3=0
calendarlength=TidyCalendarCost_df['DayInYear'].max()-TidyCalendarCost_df['DayInYear'].min()

for row in TidyCalendarCost_df.groupby(['hash_Instance']).count().sort_values('Account').groupby('Account'):
    sum1=sum1+len(row[1])
    sum2=sum2+len(row[1])*row[0]
    sum3=sum3+(calendarlength-row[0])*(len(row[1]))

    print('   -il y a '+str(len(row[1]))+' instances de '+str(row[0])+' jours')


    print(sum3)

print('Il manque '+str(sum3)+' dates')

toc=time.perf_counter()

print('temps mis '+str(toc-tic))
# %%
# Consolidation des data -> Creation d'instance ID
# ---
price=0
'''
for row in RawCalendarCost_df.iterrows():

        row=row[1]
        price=price+row['Amount']
'''

time_for=list()
time_for.append(0)

time_table=list()
time_table.append(0)

time_inTable=list()
time_inTable.append(0)

time_add=list()
time_add.append(0)

nameColumns=list(TidyCalendarCost_df.columns[0:6])

InstanceConversion_df=pd.DataFrame(columns = nameColumns)

row=TidyCalendarCost_df.iloc[0]
ID_Account=1
ID_Service=1
#new_row = {'ID_Instance':1,'hash_Instance':row['hash_Instance'],'Account':row['Account'], 'Service':row['Service']}
new_row = {'ID_Instance':1,'hash_Instance':row['hash_Instance'],'ID_Account':ID_Account,'ID_Service':ID_Service,'Account':row['Account'],'Service':row['Service']}
InstanceConversion_df=InstanceConversion_df.append(new_row,ignore_index=True)



ticInit=time.perf_counter()


hashList=list()
hashList.append(row['hash_Instance'])


accountList=list()
accountList.append(row['Account'])

serviceList=list()
serviceList.append(row['Service'])


for idx,row in enumerate(TidyCalendarCost_df.iterrows()):
        ticForStart=time.perf_counter()
        row=row[1]
        toStock=False
        bHashExists=False
        #print('row du df')
        #print(row)

        bHashExists =row['hash_Instance'] in hashList

        bAccountExists =row['Account'] in accountList
        bServiceExists =row['Service'] in serviceList

        #bHashExists = any(InstanceConversion_df.hash.str.contains(row['hash']))
        '''
        for name in InstanceConversion_df.iterrows():
            ticInTableStart=time.perf_counter()
            name=name[1]
            #print(name)
            if (string_comparison(row['Service'],name['Service']) and string_comparison(row['Account'],name['Account'])):
                toStock=True
                break

            ticInTableEnd=time.perf_counter()
            time_inTable.append(time_inTable[-1]+ticInTableEnd-ticInTableStart)
        '''
        ticTableEnd=time.perf_counter()
        time_table.append(ticTableEnd-ticForStart)


        time_addStart=time.perf_counter()

        if not bHashExists:

            if bAccountExists:
                thisAccount=InstanceConversion_df.loc[InstanceConversion_df['Account']==row['Account']]['ID_Account']
                ID_Account=np.mean(thisAccount)
            else:
                accountList.append(row['Account'])
                InstanceConversion_df['ID_Account'].astype(int)
                ID_Account=InstanceConversion_df['ID_Account'].max()+1

            if bServiceExists:
                thisService=InstanceConversion_df.loc[InstanceConversion_df['Service']==row['Service']]['ID_Service']
                ID_Service=np.mean(thisService)
            else:
                serviceList.append(row['Service'])
                InstanceConversion_df['ID_Service'].astype(int)
                ID_Service=InstanceConversion_df['ID_Service'].max()+1

            hashList.append(row['hash_Instance'])
            #new_row = {'ID_Instance':len(hashList),'hash_Instance':row['hash_Instance'],'Account':row['Account'], 'Service':row['Service']}
            #new_row = {'ID_Instance':len(hashList),'hash_Instance':row['hash_Instance'],'ID_Account':ID_Account,'Account':row['Account'], 'Service':row['Service']}
            new_row ={'ID_Instance':len(hashList),'hash_Instance':row['hash_Instance'],'ID_Account':ID_Account,'ID_Service':ID_Service,'Account':row['Account'],'Service':row['Service']}
            InstanceConversion_df=InstanceConversion_df.append(new_row,ignore_index=True)




        ticForEnd = time.perf_counter()

        time_add.append(ticForEnd-time_addStart)

        time_for.append(ticForEnd-ticForStart)


#combined_df = pd.concat(list_of_dataframes)


ticEnd=time.perf_counter()

total_time=ticEnd-ticInit

toc=time.perf_counter()
print('temps mis '+str(toc-tic))

# %
# Ajout des ID_Instance à TidyCalendarCost_df
# ----------------
row=InstanceConversion_df.iloc[0]




for instance in InstanceConversion_df.iterrows():

    TidyCalendarCost_df.loc[lambda TidyCalendarCost_df: TidyCalendarCost_df['hash_Instance'] == instance[1]['hash_Instance'],
['ID_Instance']]=instance[1]['ID_Instance']

    TidyCalendarCost_df.loc[lambda TidyCalendarCost_df: TidyCalendarCost_df['Account'] == instance[1]['Account'],
['ID_Account']]=instance[1]['ID_Account']

    TidyCalendarCost_df.loc[lambda TidyCalendarCost_df: TidyCalendarCost_df['Service'] == instance[1]['Service'],
['ID_Service']]=instance[1]['ID_Service']

# %
# On supprime tout le service Refound
# ----------------
TidyCalendarCost_df=TidyCalendarCost_df.loc[TidyCalendarCost_df['Service']!='Refund']

TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['hash_Instance'])
TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['Account'])
TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['Service'])

InstanceConversion_df=InstanceConversion_df.drop(columns=['hash_Instance'])
nameInstanceConversion_df=InstanceConversion_df
# %%
# Affichage des temps de calcul
# ----------------
'''
plt.plot(time_table,label='temps de recherche dans la table de conversion')
plt.plot(time_add,label='temps d`ajout à la table de conversion')
plt.plot(time_for,label='temps total par element')

plt.xlabel('number of row in data')
plt.ylabel('temspin s')
plt.title('cumulated calculation time')
plt.legend()

sTable='parcours de la table de conversion: '+str(round(time_table[-1],1))+'s'
sAdd='ajout d`un élément à la table de conversion: '+str(round(time_add[-1],1))+'s'
sFor='total par élément du dataframe: '+str(round(time_for[-1],1))+'s'
sAll='total du temps de calcul: '+str(round(total_time,1))+'s'
sToPrint=sAll

plt.text(2*len(time_table)/3, 2*(time_add[-1]),sToPrint,
        verticalalignment='center', horizontalalignment='left',
        color='green', fontsize=8)
plt.show()
'''

# %%
# Consolidation des data  -> creation d'ID Day
# ---

sYear=TidyCalendarCost_df['Year'].astype("string")
#sMonth défini au début
sDay=TidyCalendarCost_df['DayOfMonth'].astype("string")
#sDayName=pd.to_datetime(TidyCalendarCost_df['Period Start']).dt.day_name()


TidyCalendarCost_df.insert (4, 'hash_Day', sYear+sMonth+sDay)

TidyCalendarCost_df.insert (4, 'ID_Day', TidyCalendarCost_df['Year']*0)

nameColumns=['ID_Day','hash_Day','Year', 'Month','DayInYear','DayOfMonth','DayOfWeek']
#nameColumns=list(TidyCalendarCost_df.columns[4:6])

CalendarConversion_df=pd.DataFrame(columns = nameColumns)

row=TidyCalendarCost_df.iloc[0]
new_row = {'ID_Day':1,'hash_Day':row['hash_Day'],'Year':row['Year'], 'Month':row['Month'],'DayInYear':row['DayInYear'],'DayOfMonth':row['DayOfMonth'],'DayOfWeek':row['DayOfWeek']}
CalendarConversion_df=CalendarConversion_df.append(new_row,ignore_index=True)


hashList=list()
hashList.append(row['hash_Day'])

for idx,row in enumerate(TidyCalendarCost_df.iterrows()):
    row=row[1]
    bHashExists=False
    bHashExists =row['hash_Day'] in hashList

    if not bHashExists:
        hashList.append(row['hash_Day'])
        new_row = {'ID_Day':len(CalendarConversion_df),'hash_Day':row['hash_Day'],'Year':row['Year'], 'Month':row['Month'],'DayInYear':row['DayInYear'],'DayOfMonth':row['DayOfMonth'],'DayOfWeek':row['DayOfWeek'] }

        CalendarConversion_df=CalendarConversion_df.append(new_row,ignore_index=True)

# Ajout des ID_Day à TidyCalendarCost_df
# ----------------

for day in CalendarConversion_df.iterrows():
    TidyCalendarCost_df.loc[TidyCalendarCost_df['hash_Day'] == day[1]['hash_Day'],
['ID_Day']]=day[1]['ID_Day']

    #TidyCalendarCost_df.loc[lambda TidyCalendarCost_df: TidyCalendarCost_df['hash_Day'] == day[1]['hash_Day'],
#['ID_Day']]=day[1]['ID_Day']

TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['hash_Day'])
CalendarConversion_df=CalendarConversion_df.drop(columns=['hash_Day'])

# %%

#CalendarConversion_df=CalendarConversion_df.head(20)
#InstanceConversion_df=InstanceConversion_df.head(20)
StockedCalendarCost_df=TidyCalendarCost_df
# %%

InstanceConversion_df=InstanceConversion_df.drop(columns=['Account'])
InstanceConversion_df=InstanceConversion_df.drop(columns=['Service'])


#TidyCalendarCost_df=TidyCalendarCost_df.head(1000)

CompleteCalendarCost_df=TidyCalendarCost_df

ticStart=time.perf_counter()

sum_ajout=0

temp_prep1=0
temp_prep2=0
temp_prep2_1=0
temp_prep2_2=0
temp_test=0
temp_add=0
temp_add_1=0
temp_add_2=0
temp_add_3=0
temp_for=0

#table_of_dataframes=np.array(str(sum3),len(CompleteCalendarCost_df.columns))

list_of_frames=[]

for day in CalendarConversion_df.iterrows():

    tic_1=time.perf_counter()

    day=day[1]


    #thisDayInstances_df=CompleteCalendarCost_df.loc[CompleteCalendarCost_df['ID_Day'] == day['ID_Day']]

    #thisDayCara=thisDayInstances_df.head(1)[CalendarConversion_df.columns]
    #thisDayCara=thisDayCara.set_index(pd.Index([0]))

    #thisDayCara=pd.DataFrame(day.values,index=day.index).T

    thisDayCara=day.values

    Id_Instance_List=list(CompleteCalendarCost_df.loc[CompleteCalendarCost_df['ID_Day'] == day['ID_Day']]['ID_Instance'])




    bIDExists=False

    tic_2=time.perf_counter()

    for instance in InstanceConversion_df.iterrows():

        tic_3=time.perf_counter()

        instance=instance[1]


        tic_3_1=time.perf_counter()

        #thisInstanceCara=pd.DataFrame(instance.values,index=instance.index).T

        thisInstanceCara=instance.values
        #print(instance['ID_Instance'])
        tic_4=time.perf_counter()

        bIDExists = instance['ID_Instance'] in Id_Instance_List

        tic_5=time.perf_counter()

        if not bIDExists:

            tic_5_1=time.perf_counter()

            sum_ajout=sum_ajout+1

            Id_Instance_List.append(instance['ID_Instance'])

            tic_5_2=time.perf_counter()

            #Amount=pd.DataFrame([0],columns=['Amount'])

            Amount=[0]

            new_row=np.concatenate([thisInstanceCara,Amount,thisDayCara])

            #table_of_dataframes[]

            tic_5_3=time.perf_counter()

            list_of_frames.append(np.transpose(new_row))
            #CompleteCalendarCost_df=CompleteCalendarCost_df.append(new_row,ignore_index=True)

            tic_5_4=time.perf_counter()

            temp_add_1=temp_add_1+tic_5_2-tic_5_1
            temp_add_2=temp_add_2+tic_5_3-tic_5_2
            temp_add_3=temp_add_3+tic_5_4-tic_5_3

        tic_6=time.perf_counter()

        temp_prep2=temp_prep2+tic_4-tic_3
        temp_prep2_1=temp_prep2_1+tic_3_1-tic_3
        temp_prep2_2=temp_prep2_2+tic_4-tic_3_1

        temp_test=temp_test+tic_5-tic_4

        temp_add=temp_add+tic_6-tic_5


    tic_3=time.perf_counter()


    temp_prep1=temp_prep1+tic_2-tic_1
    temp_for=temp_for+tic_3-tic_2

    if temp_prep1+temp_for>400:
        print('force exit : t>400 s')
        break


temp_create=0
tic_7=time.perf_counter()
list_of_dataframes=[]
for row in list_of_frames:
    #CompleteCalendarCost_df=CompleteCalendarCost_df.append(pd.DataFrame(row,index=CompleteCalendarCost_df.columns).T)
    list_of_dataframes.append(pd.DataFrame(row,index=CompleteCalendarCost_df.columns).T)

CompleteCalendarCost_df=CompleteCalendarCost_df.append(pd.concat(list_of_dataframes),ignore_index=True)

tic_8=time.perf_counter()
temp_create=temp_create+tic_8-tic_7

#TidyCalendarCost_df=TidyCalendarCost_df.drop(columns=['Period Start'])

ticEnd=time.perf_counter()

print('on a ajouté'+str(sum_ajout)+'pour des valeurs vides')

total_time=ticEnd-ticStart
print(total_time)



# %%
# Valeur négative
# ----------------

negCalendarCost_df=CompleteCalendarCost_df.loc[CompleteCalendarCost_df['Amount']<0]

listNameServiceNeg=''
listNameAccountNeg=''

for row in negCalendarCost_df.iterrows():
    row=row[1]
    listNameServiceNeg=listNameServiceNeg+';'+(nameInstanceConversion_df.loc[nameInstanceConversion_df['ID_Service']==row['ID_Service']].head(1).Service.values)

    listNameAccountNeg=listNameAccountNeg+';'+(nameInstanceConversion_df.loc[nameInstanceConversion_df['ID_Account']==row['ID_Account']].head(1).Account.values)

print('Il y a '+str(len(negCalendarCost_df))+' valeurs négatives venant des service : '+listNameServiceNeg+' et des accounts : '+listNameAccountNeg)

print('On met à 0 les valeurs négatives')
CompleteCalendarCost_df.loc[CompleteCalendarCost_df.Amount<0, 'Amount'] = 0

# %%
# Affichage des valeurs - tout en loi normal
# ----------------

sortedCalendarCost_df=CompleteCalendarCost_df.sort_values(['Amount'])

#Creating a Function.
def normal_dist(x , mean , sd):
    prob_density = (np.pi*sd) * np.exp(-0.5*((x-mean)/sd)**2)
    return prob_density

#Calculate mean and Standard deviation.
x=sortedCalendarCost_df['Amount'].values
x=x.astype(float)
mean = np.mean(x)
sd = np.std(x)

#Apply function to the data.
pdf = normal_dist(x,mean,sd)

#Plotting the Results
plt.plot(x,pdf , color = 'red')
plt.xlabel('Data points')
plt.ylabel('Probability Density')
#plt.xscale('log')
plt.show()

## %#
# Normalisation des Amounts
# ----------------
CompleteCalendarCost_df['Amount']=CompleteCalendarCost_df['Amount'].astype(float)

min=CompleteCalendarCost_df['Amount'].min()
max=CompleteCalendarCost_df['Amount'].max()

NormedCalendarCost_df=copy.copy(CompleteCalendarCost_df)


#Normalisation brute par les outlier -> squeezing of data
#NormedCalendarCost_df['Amount']=(CompleteCalendarCost_df['Amount']-min)/((max-min))

CompleteCalendarCost_df['Amount'].describe()

#Winsorize
from scipy.stats.mstats import winsorize

maxWinsor=0.001
NormedCalendarCost_df['Amount']=winsorize(CompleteCalendarCost_df['Amount'],limits=[0,maxWinsor])

print(str(maxWinsor*100)+'% des données max ont étés tronquées' )
print('la valeur max est maintenant '+str(NormedCalendarCost_df['Amount'].max()))
print('Il y en a  '+str(len(NormedCalendarCost_df.loc[NormedCalendarCost_df['Amount']==NormedCalendarCost_df['Amount'].max()])))

NormedCalendarCost_df['Amount'].describe()

sortedNormed=NormedCalendarCost_df.sort_values(['Amount'])
# %%
# Affichage des valeurs - tout en loi normal
# ----------------

sortedCalendarCost_df=NormedCalendarCost_df.sort_values(['Amount'])

#Creating a Function.
def normal_dist(x , mean , sd):
    prob_density = (np.pi*sd) * np.exp(-0.5*((x-mean)/sd)**2)
    return prob_density

#Calculate mean and Standard deviation.
x=sortedCalendarCost_df['Amount'].values
x=x.astype(float)
mean = np.mean(x)
sd = np.std(x)

#Apply function to the data.
pdf = normal_dist(x,mean,sd)

#Plotting the Results
plt.plot(x,pdf , color = 'red')
plt.xlabel('Data points')
plt.ylabel('Probability Density')
#plt.xscale('log')
plt.show()

## %#
# Affichage des valeurs - jour de la semaine
# ----------------



CompleteCalendarCost_df.describe()


sortedCalendarCost=CompleteCalendarCost_df.groupby('DayOfWeek').Amount.describe(exclude=[object])

'''

g1 = sns.catplot(x="DayOfMonth", y="Amount", hue="DayOfWeek", data=CompleteCalendarCost_df)
plt.show()

g1.set(ylim=(-10, 500))


# %
# Affichage des valeurs - service
# ----------------

g2 = sns.catplot(x="ID_Service", y="Amount", data=CompleteCalendarCost_df)
g2.set_xticklabels(rotation=90)
plt.show()

g2.set(ylim=(-10, 500))



# %
# Affichage des valeurs - account
# ----------------

g3 = sns.catplot(x="ID_Account", y="Amount", data=CompleteCalendarCost_df)
g3.set_xticklabels(rotation=90)
plt.show()

g3.set(ylim=(-10, 1500))


# %
# Affichage des valeurs - mois
# ----------------

g4 = sns.catplot(x="Month", y="Amount", data=CompleteCalendarCost_df)
g4.set_xticklabels(rotation=90)
plt.show()

g4.set(ylim=(-10, 5000))



# %
# Affichage des valeurs - jour de l'année
# ----------------

g5 = sns.catplot(x="DayInYear", y="Amount", data=CompleteCalendarCost_df)
g5.set_xticklabels(rotation=90)
plt.show()

g5.set(ylim=(-10, 5000))

# %
# Affichage des valeurs - 1 instance jour de l'année
# ----------------

g6 = sns.catplot(x="DayInYear", y="Amount", data=CompleteCalendarCost_df.loc[CompleteCalendarCost_df['ID_Instance']==300],hue='ID_Instance')
g6.set_xticklabels(rotation=90)
plt.show()

g6.set(ylim=(-10, 100))

'''

# %%
# Création de la data set
# ----------------
"""
def getDataSet(df):


 # creating headers
 firstInstance=pd.DataFrame(df.iloc[0:400])

 #day in week
 aWeek=firstInstance.groupby(by=['DayOfWeek'])
 thisDayDescriptionDf=aWeek.Amount.describe()
 # setting index for day
 a=list(range(len(thisDayDescriptionDf)))
 b=[str(a) for a in a]
 day_case_id=["D" + b for b in b]
 thisDayDescriptionDf.insert(0, "Id_case", day_case_id, True)
 thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="count", axis=1)
 thisDayDescriptionDf=thisDayDescriptionDf.reset_index()
 thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="DayOfWeek", axis=1)

 #day in month
 aMonth=firstInstance.groupby(by=['DayOfMonth'])
 #print(thisDf)
 thisMonthDescriptionDf=aMonth.Amount.describe()
 thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="count", axis=1)
 thisMonthDescriptionDf=thisMonthDescriptionDf.reset_index()
 thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayOfMonth", axis=1)

 # setting index for  months
 a=list(range(31))
 b=[str(a) for a in a]
 month_case_id=["M" + b for b in b]

 #print(thisMonthDescriptionDf)

 if thisMonthDescriptionDf['std'].isnull().values.any():
  thisMonthDescriptionDf['std']=thisMonthDescriptionDf['mean']

 #thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayOfMonth", axis=1)

 for dayMissing in range(31-len(thisMonthDescriptionDf)):
  #
  idMissing=dayMissing+len(thisMonthDescriptionDf)
  meanDay=thisMonthDescriptionDf.mean()
  #print(np.transpose(meanDay))
  thisMonthDescriptionDf=thisMonthDescriptionDf.append(meanDay,ignore_index=True)
 #print(thisMonthDescriptionDf)
 thisMonthDescriptionDf.insert(0, "Id_case", month_case_id, True)

 #thisMonthDescriptionDf=thisMonthDescriptionDf.reset_index()
 #thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="index", axis=1)

 #print(thisDayDescriptionDf)
 #print(thisMonthDescriptionDf)
 #print(thisDayDescriptionDf.columns)
 #print(thisMonthDescriptionDf.columns)

 #concatenate
 thisDescriptionDf=pd.concat([thisDayDescriptionDf,thisMonthDescriptionDf], axis=0)
 thisDescriptionDf= thisDescriptionDf.set_index('Id_case')

 thisInstanceDf=thisDescriptionDf.stack(dropna=False).to_frame().T
 thisInstanceDf['ID_Instance']=firstInstance.ID_Instance
 ds=pd.DataFrame(columns=thisInstanceDf.columns)


 #print(ds)
 for idx,instance in enumerate(df.groupby(by=['ID_Instance'])):
  #day in week
  thisDayDf=instance[1].groupby(by=['DayOfWeek'])
  #print(thisDf)
  thisDayDescriptionDf=thisDayDf.Amount.describe()
  thisDayDescriptionDf.insert(0, "Id_case", day_case_id, True)
  thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="count", axis=1)
  thisDayDescriptionDf=thisDayDescriptionDf.reset_index()
  thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="DayOfWeek", axis=1)
  #thisDayDescriptionDf['Id_case']=list(range(len(thisDayDescriptionDf)))

  #day in month
  thisMonthDf=instance[1].groupby(by=['DayOfMonth'])
  #print(len(thisMonthDf))
  thisMonthDescriptionDf=thisMonthDf.Amount.describe()
  thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="count", axis=1)
  thisMonthDescriptionDf = thisMonthDescriptionDf.reset_index()
  thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayOfMonth", axis=1)

  #si une seule valeur, pas de std donc on prend la moyenne
  if thisMonthDescriptionDf['std'].isnull().values.any():
   thisMonthDescriptionDf['std']=thisMonthDescriptionDf['mean']

  for dayMissing in range(31-len(thisMonthDescriptionDf)):

   idMissing=dayMissing+len(thisMonthDescriptionDf)
   meanDay=thisMonthDescriptionDf.mean()
   #print(np.transpose(meanDay))
   thisMonthDescriptionDf=thisMonthDescriptionDf.append(meanDay,ignore_index=True)

  thisMonthDescriptionDf.insert(0, "Id_case", month_case_id, True)

  #print(thisDayDescriptionDf.columns)
  #print(thisMonthDescriptionDf.columns)
  #concatenate
  thisDescriptionDf=pd.concat([thisDayDescriptionDf,thisMonthDescriptionDf], axis=0)
  thisDescriptionDf= thisDescriptionDf.set_index('Id_case')


  thisInstanceDf=thisDescriptionDf.stack(dropna=False).to_frame().T
  '''
  dayService=thisDayDf.Service.mean().reset_index().Service
  monthService=thisMonthDf.Service.mean().reset_index().Service
  serviceValue=np.mean(pd.concat([dayService,monthService], axis=0))

  '''
  thisInstanceDf['ID_Instance']=instance[1].head(1).ID_Instance.values.astype(int)
  thisInstanceDf['ID_Account']=instance[1].head(1).ID_Account.values.astype(int)
  thisInstanceDf['ID_Service']=instance[1].head(1).ID_Service.values.astype(int)
  #thisInstanceDf.reset_index()


  '''
  dayID=thisDayDf.ID_Instance.mean().reset_index().ID_Instance
  monthID=thisMonthDf.ID_Instance.mean().reset_index().ID_Instance
  IDValue=np.mean(pd.concat([dayID,monthID], axis=0))
  thisInstanceDf['ID_Instance']=IDValue
    '''
  #print(thisInstanceDf.columns)
  #print(thisInstanceDf)
  ds=ds.append(thisInstanceDf,ignore_index=True)

 #print(ds)

 return ds
# ----------------


#CompleteCalendarCost_df=CompleteCalendarCost_df.drop(columns=['Service','Account'])


CompleteCalendarCost_df=CompleteCalendarCost_df.astype(float)

CompleteCalendarCost_ds=getDataSet(CompleteCalendarCost_df)
"""

# %%
# Créé le jeu de data pur
# ----------------
timePeriod=31
pcentMedian=1.2

ticStart=time.perf_counter()

CalendarCost_df=NormedCalendarCost_df.astype(float)


#initialisation
#listFeatures=pd.Series(data=None,dtype=float)
listFeatures=list()
vectorWeek=np.zeros(7)
vectorMonth=np.zeros(9)

nameFeatures=np.empty(21,dtype=str)
listNameFeatures=list()

for idx,instance in enumerate(CalendarCost_df.groupby(by=['ID_Instance'])):

    instance=instance[1].sort_values(by=['ID_Day'])
    #number of day in calendar
    numCalendar=len(instance)
    #list of monday in calendar
    listMonday=list(instance.loc[instance.DayOfWeek==0]['ID_Day'])
    for idy in (listMonday):
        thisPeriod=instance.loc[(instance["ID_Day"] >= idy) & (instance["ID_Day"] < idy+timePeriod)]

        if len(thisPeriod)==timePeriod:

            if len(listNameFeatures)==0:

                #IDs Instance et jour ini
                listNameFeatures=list(['ID_Instance','ID_Account','ID_Service','ID_Day_Ini'])
                #nom jour
                listNameFeatures=listNameFeatures+list(thisPeriod['Amount'].describe().index[1:])
                #nom semaine
                a=list(range(7))
                b=[str(a) for a in a]
                listNameFeatures=listNameFeatures+["MeanD" + b for b in b]
                #nom mois
                listNameFeatures.append('sinPosMax')
                listNameFeatures.append('cosPosMax')
                listNameFeatures.append('sinPosMin')
                listNameFeatures.append('cosPosMin')
                listNameFeatures.append('percentOutsideWindow')
                listNameFeatures.append('maxIncrease')
                listNameFeatures.append('minIncrease')
                listNameFeatures.append('meanIncrease')
                listNameFeatures.append('meanAbsIncrease')


            # features des IDS
            vectorIDs=[thisPeriod['ID_Instance'].mean(),thisPeriod['ID_Account'].mean(),thisPeriod['ID_Service'].mean(),thisPeriod['ID_Day'].head(1).values[0]]

            # features du jour
            vectorDay=thisPeriod['Amount'].describe().values[1:]

            # features semaine
            for idz in range(len(vectorWeek)):
                vectorWeek[idz]=thisPeriod.loc[thisPeriod['DayOfWeek']==idz]['Amount'].describe()['50%']

            # features mois

            tableDerivative=np.zeros(timePeriod-1)
            #on code cycliquement la position max, si pas de max, on mets 0
            if len(thisPeriod['Amount']==thisPeriod['Amount'].max())==1:
                pMax=thisPeriod.loc[thisPeriod['Amount']==thisPeriod['Amount'].max()]['DayOfMonth']
            else:
                pMax=0
            vectorMonth[0]=math.sin(2*math.pi*pMax/timePeriod)
            vectorMonth[1]=math.cos(2*math.pi*pMax/timePeriod)

            #on code cycliquement la position min, si pas de max, on mets 0
            if len(thisPeriod['Amount']==thisPeriod['Amount'].min())==1:
                pMin=thisPeriod.loc[thisPeriod['Amount']==thisPeriod['Amount'].min()]['DayOfMonth']
            else:
                pMin=0
            vectorMonth[2]=math.sin(2*math.pi*pMin/timePeriod)
            vectorMonth[3]=math.cos(2*math.pi*pMin/timePeriod)

            #on calcul le nombre de points en dehors de +- pcentMedian
            if thisPeriod['Amount'].mean()>0:
                nMax=sum((thisPeriod['Amount']>thisPeriod['Amount'].mean()*pcentMedian)&(thisPeriod['Amount']<thisPeriod['Amount'].mean()/pcentMedian))
            else:
                nMax=sum((thisPeriod['Amount']<thisPeriod['Amount'].mean()*pcentMedian)&(thisPeriod['Amount']>thisPeriod['Amount'].mean()/pcentMedian))
            vectorMonth[4]=nMax/timePeriod

            #on calcul une liste de dérivé
            for ix in range(len(tableDerivative)):
                tableDerivative[ix]=thisPeriod.iloc[ix+1].Amount-thisPeriod.iloc[ix].Amount
            vectorMonth[5]=np.max(tableDerivative)
            vectorMonth[6]=np.min(tableDerivative)
            vectorMonth[7]=np.mean(tableDerivative)
            vectorMonth[8]=np.mean(abs(tableDerivative))

            #on ajoute
            seriesFeatures=np.concatenate((vectorIDs,vectorDay,vectorWeek,vectorMonth),axis=None)
            listFeatures.append(seriesFeatures)


FeaturedCalendarCost_df=pd.DataFrame(listFeatures,columns=listNameFeatures)

ticEnd=time.perf_counter()
print('on a créé:'+str(len(listFeatures))+' exemples avec '+str(len(listNameFeatures))+' features')

total_time=ticEnd-ticStart
print(total_time)



# %%
# Sauvegarder
# ----------------

import pickle


# Open a file and use dump()
with open('FeaturedCalendarCost.pkl', 'wb') as file:

    # A new file will be created
    pickle.dump(FeaturedCalendarCost_df, file)

# %%
# Charger
# ----------------

import pickle

# Open the file in binary mode
with open('FeaturedCalendarCost.pkl', 'rb') as file:

    # Call load method to deserialze
    FeaturedCalendarCost_df = pickle.load(file)



# %%
# Créé le jeu de data pur
# ----------------
#CompleteCalendarCost_ds=CompleteCalendarCost_ds.fillna(-50)
#stocked=CompleteCalendarCost_ds
stocked=FeaturedCalendarCost_df

#CompleteCalendarCost_ds=stocked
stocked['ID_Instance']=stocked['ID_Instance'].astype(int)
stocked['ID_Account']=stocked['ID_Account'].astype(int)
stocked['ID_Service']=stocked['ID_Service'].astype(int)
stocked['ID_Day_Ini']=stocked['ID_Day_Ini'].astype(int)

pureCalendarCost_ds=stocked
pureCalendarCost_ds=pureCalendarCost_ds.drop(columns='ID_Instance')
pureCalendarCost_ds=pureCalendarCost_ds.drop(columns='ID_Account')
pureCalendarCost_ds=pureCalendarCost_ds.drop(columns='ID_Service')
pureCalendarCost_ds=pureCalendarCost_ds.drop(columns='ID_Day_Ini')

#pureCalendarCost_ds=pureCalendarCost_ds.drop(columns='ID_Instance', axis = 1, level = 0)
#pureCalendarCost_ds=pureCalendarCost_ds.drop(columns='ID_Day_Ini', axis = 1, level = 0)
# %%
# Plot signature
# ----------------

'''
ds=pureCalendarCost_ds

fig = plt.figure()
ax = fig.gca(projection='3d')

zLim=np.max(ds.max(axis = 1, skipna = True))

zdata=np.empty([len(ds),len(ds.columns)])

idy=0
for idx,columns in enumerate(ds):
    zdata[:,idy] =ds[columns]
    idy=idy+1

ny, nx = zdata.shape
x = range(nx)
y = range(ny)
xv, yv = np.meshgrid(x, y)

scatter = ax.plot_surface(xv,yv,zdata,rstride=1, cstride=1,
                cmap='gist_gray', edgecolor='none')

ax.set_xlabel("Signatures")
ax.axes.xaxis.set_ticks([])
ax.set_ylabel("jeu d'instance")
ax.axes.yaxis.set_ticks([])
ax.set_zlim(0, zLim)
ax.set_title('All series')


plt.show()
'''

# %%
# MeanShift
# ----------------
from sklearn.cluster import MeanShift, estimate_bandwidth

#CompleteCalendarCost_ds=CompleteCalendarCost_ds.astype(float)
MeanShiftCalendarCost_ds=pureCalendarCost_ds
#
X=MeanShiftCalendarCost_ds.values

bandwidth = estimate_bandwidth(X, quantile=0.01, n_samples=5000)


clustering = MeanShift().fit(MeanShiftCalendarCost_ds.values)


ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)

ms.fit(X)

labels = ms.labels_
cluster_centers = ms.cluster_centers_

labels_unique = np.unique(labels)
n_clusters_ = len(labels_unique)



## Plot result : heat map

heatMap_Array=np.zeros((stocked['ID_Service'].astype(int).max(),stocked['ID_Account'].astype(int).max()),dtype=int)

fig, ax = plt.subplots()


for idx in range(stocked['ID_Service'].astype(int).max()):

    testAccount=stocked.loc[stocked['ID_Service']==idx+1]

    for idy in range(stocked['ID_Account'].astype(int).max()):

        if (idy+1) in testAccount['ID_Account'].values:

            testAccountService=testAccount.loc[testAccount['ID_Account']==idy+1]

            testAccountService=testAccountService.drop(columns='ID_Account')

            testAccountService=testAccountService.drop(columns='ID_Service')

            testAccountService=testAccountService.drop(columns='ID_Instance')

            testAccountService=testAccountService.drop(columns='ID_Day_Ini')

            #testAccountService=testAccountService.drop(columns='ID_Cluster')

            heatMap_Array[idx,idy]=len(np.unique(clustering.predict(testAccountService.values)))

            #if np.mean(clustering.predict(testAccountService.values))!=0:
            text = ax.text(idx,idy, np.transpose(heatMap_Array[idx,idy]),ha="center", va="center", color="k")

        else:

            heatMap_Array[idx,idy]=-10

im = ax.imshow(np.transpose(heatMap_Array),cmap='RdBu')
ax.set_title('Mean Shift result : Nombre de cluster par Instance/periode')
ax.set_ylabel("Numéro du Account")
ax.set_ylabel("Numéro du Service")

fig.tight_layout()
plt.show()

## Plot result : heat map

heatMap_Array=np.zeros((stocked['ID_Service'].astype(int).max(),stocked['ID_Account'].astype(int).max()),dtype=int)

fig, ax = plt.subplots()


for idx in range(stocked['ID_Service'].astype(int).max()):

    testAccount=stocked.loc[stocked['ID_Service']==idx+1]

    for idy in range(stocked['ID_Account'].astype(int).max()):

        if (idy+1) in testAccount['ID_Account'].values:

            testAccountService=testAccount.loc[testAccount['ID_Account']==idy+1]

            testAccountService=testAccountService.drop(columns='ID_Account')

            testAccountService=testAccountService.drop(columns='ID_Service')

            testAccountService=testAccountService.drop(columns='ID_Instance')

            testAccountService=testAccountService.drop(columns='ID_Day_Ini')

            #testAccountService=testAccountService.drop(columns='ID_Cluster')

            heatMap_Array[idx,idy]=np.mean(clustering.predict(testAccountService.values))

            #if np.mean(clustering.predict(testAccountService.values))!=0:
            text = ax.text(idx,idy, np.transpose(heatMap_Array[idx,idy]),ha="center", va="center", color="k")

        else:

            heatMap_Array[idx,idy]=-10

im = ax.imshow(np.transpose(heatMap_Array),cmap='jet')
ax.set_title('Mean Shift result : Numero moyen des clusters par Instance/periode')
ax.set_ylabel("Numéro du Account")
ax.set_ylabel("Numéro du Service")

fig.tight_layout()
plt.show()



# %%
# K-means
# ----------------

from sklearn.cluster import KMeans

KMeansCalendarCost_ds=pureCalendarCost_ds

X=KMeansCalendarCost_ds.values

n_clusters = 200

kmeans = KMeans(n_clusters, random_state=0).fit(X)


## Plot result : heat map

heatMap_Array=np.zeros((stocked['ID_Service'].astype(int).max(),stocked['ID_Account'].astype(int).max()),dtype=int)

fig, ax = plt.subplots()


for idx in range(stocked['ID_Service'].astype(int).max()):

    testAccount=stocked.loc[stocked['ID_Service']==idx+1]

    for idy in range(stocked['ID_Account'].astype(int).max()):

        if (idy+1) in testAccount['ID_Account'].values:

            testAccountService=testAccount.loc[testAccount['ID_Account']==idy+1]

            testAccountService=testAccountService.drop(columns='ID_Account')

            testAccountService=testAccountService.drop(columns='ID_Service')

            testAccountService=testAccountService.drop(columns='ID_Instance')

            testAccountService=testAccountService.drop(columns='ID_Day_Ini')

            #testAccountService=testAccountService.drop(columns='ID_Cluster')

            heatMap_Array[idx,idy]=np.mean(kmeans.predict(testAccountService.values))


            if np.mean(kmeans.predict(testAccountService.values))!=0:
                text = ax.text(idx,idy, np.transpose(heatMap_Array[idx,idy]),
                       ha="center", va="center", color="b")

        else:

            heatMap_Array[idx,idy]=-10

im = ax.imshow(np.transpose(heatMap_Array),cmap='hot')
ax.set_title('K-means result : Cluster moyen trouvé')
ax.set_ylabel("Numéro du Account")
ax.set_xlabel("Numéro du Service")

fig.tight_layout()
plt.show()

## Plot result : heat map

heatMap_Array=np.zeros((stocked['ID_Service'].astype(int).max(),stocked['ID_Account'].astype(int).max()),dtype=int)

listRepartionHeatMap=list()

fig, ax = plt.subplots()


for idx in range(stocked['ID_Service'].astype(int).max()):

    testAccount=stocked.loc[stocked['ID_Service']==idx+1]

    for idy in range(stocked['ID_Account'].astype(int).max()):

        if (idy+1) in testAccount['ID_Account'].values:

            testAccountService=testAccount.loc[testAccount['ID_Account']==idy+1]

            testAccountService=testAccountService.drop(columns='ID_Account')

            testAccountService=testAccountService.drop(columns='ID_Service')

            testAccountService=testAccountService.drop(columns='ID_Instance')

            testAccountService=testAccountService.drop(columns='ID_Day_Ini')

            #testAccountService=testAccountService.drop(columns='ID_Cluster')

            heatMap_Array[idx,idy]=len(np.unique(kmeans.predict(testAccountService.values)))

            if len(np.unique(kmeans.predict(testAccountService.values)))>2:
                listRepartionHeatMap.append(len(np.unique(kmeans.predict(testAccountService.values))))

            #if np.mean(clustering.predict(testAccountService.values))!=0:
            text = ax.text(idx,idy, np.transpose(heatMap_Array[idx,idy]),ha="center", va="center", color="k")

        else:

            heatMap_Array[idx,idy]=-10

im = ax.imshow(np.transpose(heatMap_Array),cmap='jet')
ax.set_title('Mean Shift result : Nombre de cluster par Instance/periode')
ax.set_ylabel("Numéro du Account")
ax.set_xlabel("Numéro du Service")

fig.tight_layout()
plt.show()


## Plot result : heat map

listRepartionHeatMap.sort()
RepartionHeatMap=pd.DataFrame(listRepartionHeatMap)

listX=copy.copy(listRepartionHeatMap)

listCluster=list(range(n_clusters))


heatRepartitionMap_Array=np.zeros((len(listRepartionHeatMap),n_clusters),dtype=int)

fig, ax = plt.subplots()



for idx in range(stocked['ID_Service'].astype(int).max()):

    testAccount=stocked.loc[stocked['ID_Service']==idx+1]

    for idy in range(stocked['ID_Account'].astype(int).max()):

        if (idy+1) in testAccount['ID_Account'].values:

            testAccountService=testAccount.loc[testAccount['ID_Account']==idy+1]

            testAccountService=testAccountService.drop(columns='ID_Account')

            testAccountService=testAccountService.drop(columns='ID_Service')

            testAccountService=testAccountService.drop(columns='ID_Instance')

            testAccountService=testAccountService.drop(columns='ID_Day_Ini')

            #testAccountService=testAccountService.drop(columns='ID_Cluster')

            arrayPredict=kmeans.predict(testAccountService.values)

            if len(np.unique(arrayPredict))>2 :

                num=len(np.unique(arrayPredict))
                x=listX.index(num)
                listX.remove(num)

                for idy2 in listCluster:
                    heatRepartitionMap_Array[x-2,idy2]=np.count_nonzero(arrayPredict == idy2)
                   # text = ax.text(x,idy2, np.transpose(heatRepartitionMap_Array[x,idy2]),ha="center", va="center", color="k")


im = ax.imshow(np.transpose(heatRepartitionMap_Array),cmap='jet')
ax.set_title('Mean Shift result : Nombre de cluster par Instance/periode')
ax.set_ylabel("Numéro du Cluster")
ax.set_ylabel("Nombre d'instance-periode'")
ax.set_xticks(np.arange(len(listRepartionHeatMap)))
ax.set_xticklabels(listRepartionHeatMap)

fig.tight_layout()
plt.show()


## Plot result : heat map

listRepartionHeatMap.sort()
RepartionHeatMap=pd.DataFrame(listRepartionHeatMap)

listX=copy.copy(listRepartionHeatMap)

listCluster=list(range(n_clusters))


heatRepartitionMap_Array=np.zeros((len(listRepartionHeatMap),max(listRepartionHeatMap)),dtype=int)

fig, ax = plt.subplots()



for idx in range(stocked['ID_Service'].astype(int).max()):

    testAccount=stocked.loc[stocked['ID_Service']==idx+1]

    for idy in range(stocked['ID_Account'].astype(int).max()):

        if (idy+1) in testAccount['ID_Account'].values:

            testAccountService=testAccount.loc[testAccount['ID_Account']==idy+1]

            testAccountService=testAccountService.drop(columns='ID_Account')

            testAccountService=testAccountService.drop(columns='ID_Service')

            testAccountService=testAccountService.drop(columns='ID_Instance')

            testAccountService=testAccountService.drop(columns='ID_Day_Ini')

            testAccountService=testAccountService.drop(columns='ID_Cluster')

            arrayPredict=kmeans.predict(testAccountService.values)

            if len(np.unique(arrayPredict))>1 :

                num=len(np.unique(arrayPredict))
                x=listX.index(num)
                listX.remove(num)

                for idy2 in listCluster:
                    heatRepartitionMap_Array[x-2,idy2]=np.count_nonzero(arrayPredict == idy2)
                   # text = ax.text(x,idy2, np.transpose(heatRepartitionMap_Array[x,idy2]),ha="center", va="center", color="k")


im = ax.imshow(np.transpose(heatRepartitionMap_Array),cmap='jet')
ax.set_title('Mean Shift result : Nombre de cluster par Instance/periode')
ax.set_ylabel("Numéro du Cluster")
ax.set_ylabel("Nombre d'instance-periode'")
ax.set_xticks(np.arange(len(listRepartionHeatMap)))

ax.set_xlabels(listRepartionHeatMap)

fig.tight_layout()
plt.show()


# %%
# Plot signature
# ----------------

listCluster=list()

tableCluster=np.zeros(n_clusters)

for row in pureCalendarCost_ds.iterrows():
    row=row[1]

    #print(int(kmeans.predict(([row.values]))))


    tableCluster[int(kmeans.predict(([row.values])))]=tableCluster[int(kmeans.predict(([row.values])))]+1

    listCluster.append(int(kmeans.predict(([row.values]))))

clusterPartion_df=pd.DataFrame(tableCluster,list(range(n_clusters)))

stocked['ID_Cluster']=listCluster


for cluster in range(0,n_clusters):




    i_cluster=cluster

    ds=pureCalendarCost_ds.loc[stocked['ID_Cluster']==i_cluster]

    zMaxlim=ds.values.max()
    zMinlim=ds.values.min()

    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib.collections import PolyCollection,LineCollection
    from matplotlib import colors as mcolors
    import matplotlib._color_data as mcd

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    def cc(arg):
        return mcolors.to_rgba(arg, alpha=0.6)

    #xs = np.arange(0, 10, 0.4)
    overlap = {name for name in mcd.CSS4_COLORS
        if "xkcd:" + name in mcd.XKCD_COLORS}

    '''
    def get_my_colors(length):

        overlap = {name for name in mcd.CSS4_COLORS
                if "xkcd:" + name in mcd.XKCD_COLORS}

        if length<=len(overlap):

        else:

            myColors=np.concatenate(list(overlap),get_my_colors(length-len(overlap),overlap)
        return myColors
    '''

    xs=np.arange(0, len(list(ds.head(1)))+2, 1)

    verts = []

    zs =np.arange(0, len(ds), 1)

    myColors=list()
    if len(ds)<40:

        myColors=list(overlap)[:len(ds)]
    else:
        myColors=['r']*len(ds)



    for row in ds.iterrows():
        row=row[1]
        ys=row.values
        ys = np.insert(ys,0,0)
        ys = np.insert(ys,-1,0)
        verts.append(list(zip(xs, ys)))

    poly = PolyCollection(verts, facecolors=myColors)
    poly.set_alpha(0.7)

    ax.add_collection3d(poly, zs=zs, zdir='y')



    '''
    xs=np.arange(0, len(list(ds.head(1)))+2, 1)
    verts = []
    ys =np.arange(0, len(ds), 1)
    myColors=list()
    myColors=list(overlap)[:len(ds)]
    for row in ds.iterrows():
        row=row[1]
        zs=row.values
        zs = np.insert(zs,0,0)
        zs = np.insert(zs,-1,0)
        verts.append(list(zip(xs, zs)))

    poly = LineCollection(verts, facecolors=myColors)
    poly.set_alpha(0.7)

    ax.add_collection3d(poly)
    '''
    ax.set_title('Cluster n°'+str(i_cluster))
    ax.set_xlabel('X')
    ax.set_xlim3d(-1, len(xs))
    ax.set_ylabel('Y')
    ax.set_ylim3d(-1, len(zs))
    ax.set_zlabel('Z')
    ax.set_zlim3d(zMinlim,zMaxlim)

    plt.show()






