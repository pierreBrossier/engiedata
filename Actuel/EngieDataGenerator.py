"""
===========================================================
 Generating engie calendar-cost dataset
===========================================================
Pierre Brossier
date : 14-05-2021

This fonction creates and return an example of calendar-cost dataset.
A single panda dataframe is returned


=========== ========================================================
Access to files :
"""
print(__doc__)

# ----------------------
# Third-party libraries
# ----------------------
import numpy as np
import pandas as pd
import math
import os
import sys
import copy
import csv
import random
import h5py
import calendar
import matplotlib.pyplot as plt
# ----------------------
# Re définition du chemin d'accès root
# ----------------------
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
print('\n')

# ----------------------
# Import Jour ferié
# ----------------------
df = pd.read_csv('jours_feries_metropole.csv')
# ----------------------

'''
# Re définition du chemin d'accès root
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
# ----------------------
'''

def getEngieDataSet(nUnitInput,yearToDateInput,monthToDateInput):


        # ----------------------
        # Creation du calendrier
        # ----------------------
        '''
        version 2 :
        Ajout des jours fériés
        Modifier le focus sur un an
        '''

        CalendarGenerator=calendar.Calendar()

        sNameCalendar=np.array([['Year','DayInYear','Month','DayInMonth','Week','DayInWeek','variability','bNotHolliday']])

        fCalendar=[np.zeros(len(sNameCalendar[0]))]

        # input du mois/année où on en est :
        yearToDate=yearToDateInput
        monthToDate=monthToDateInput
        rangeYears=[yearToDate-1,yearToDate]
        nonWorkingDateYear1=df[df['annee']==yearToDate-1]['date']
        nonWorkingDateYear1=nonWorkingDateYear1.append(df[df['annee']==yearToDate]['date'],'date')
        #print(nonWorkingDateYear1)
        nonWorkingDateYear=pd.DataFrame()
        nonWorkingDateYear['dayInYear']=pd.to_datetime(nonWorkingDateYear1).dt.dayofyear
        nonWorkingDateYear['year']=pd.to_datetime(nonWorkingDateYear1).dt.year


        #print(nonWorkingDateYear)

        #on remplis les deux ans sans distinction pour compter les jours et les semaines
        bHolliday=True
        for itYears in range(2):
                thisCalendrier=CalendarGenerator.yeardays2calendar(rangeYears[itYears],1) #un seul mois par ligne, sinon 3 mois de base
                itMonth=0
                DayInWeek=0
                itWeek=0
                itDayInYear=0
                pYear=random.gauss(1,0.1)
                for month in thisCalendrier:
                        month=month[0]
                        itMonth=itMonth+1
                        for week in month:

                                if DayInWeek==6:
                                        itWeek=itWeek+1

                                pMonth=random.gauss(1,0.2)
                                for day in week:
                                        if day[0]!=0:
                                                itDayInYear=itDayInYear+1
                                                DayInMonth=day[0]
                                                DayInWeek=day[1]
                                                pDay=random.gauss(1,0.3)
                                                years=nonWorkingDateYear[nonWorkingDateYear['dayInYear']==itDayInYear]['year']
                                                bHolliday=True
                                                for year in years:
                                                        if (year==rangeYears[itYears]):
                                                                bHolliday=False

                                                if (fCalendar[0][0])==0:
                                                        fCalendar=[[rangeYears[0]+itYears,itDayInYear,itMonth,DayInMonth,itWeek,DayInWeek,pYear*pMonth*pDay,bHolliday]]
                                                else:
                                                        fCalendar=np.append(fCalendar,[[rangeYears[itYears],itDayInYear,itMonth,DayInMonth,itWeek,DayInWeek,pYear*pMonth*pDay,bHolliday]],axis=0)



        fCalendar=np.array(fCalendar)
        #print(sum(fCalendar[:,-1]))

        #On selectionne 1 an à partir du mois utilisé
        if monthToDate==1:
                debut=0
                fin=np.where((fCalendar[:,0]==yearToDate-1))[0][-1]
        else:
                debut=np.where((fCalendar[:,0]==yearToDate-1) & (fCalendar[:,2] == monthToDate))[0][0]
                fin=np.where((fCalendar[:,0]==yearToDate) & (fCalendar[:,2] == monthToDate))[0][1]

        calendarDebut,fCalendar,calendarFin=np.split(fCalendar,[debut,fin], axis=0)

        print('année de '+str(len(fCalendar))+' jours')
        print('\n')
        #print(sum(fCalendar[:,-1]))

        # ----------------------
        # Creation des données de prix fictive par service et unité
        # ----------------------


        #with h5py.File("monOrganisation.hdf5", "w") as file:

        #dset = file.create_dataset("mydataset")
        #dset.attrs['cout'] = 0

        nUnit=nUnitInput
        listUnit=list()
        nServiceMax=5

        #service A
        #constant
        meanPriceA=100
        intervalleA=30

        #service B
        #constant except for a value hapenning the 15th of each month
        meanPriceB=75
        intervalleB=15
        maxPriceB=600

        #service C
        #constant except the weekend
        meanPriceC=200
        intervalleC=50
        minPriceC=20

        #service D
        #oscillating between two values at random
        maxPriceD=150
        minPriceD=50
        intervalleD=20

        #service E
        #constant or null over the month
        meanPriceE=225
        intervalleE=30


        idDays=list()
        idDays.append(0)

        randomChoice=False

        GlobalCostMatrix=np.array('f')

        sNameGlobal=np.append(sNameCalendar,['Service'])
        sNameGlobal=np.append(sNameGlobal,['Price'])


        iDServiceInstance=0

        for iUnit in range (nUnit):


        #thisUnit = file.create_group("Unit"+str(iUnit))
        #listUnit.append(thisUnit)

                nService=random.randrange(1,nServiceMax)
                for iService in range (nService):

                        iDServiceInstance=iDServiceInstance+1
                        thisService=random.randrange(0,nServiceMax)
                        if thisService == 0:
                                sType='A'
                                priceA=random.uniform(meanPriceA-intervalleA/2,meanPriceA+intervalleA/2)
                                price = (fCalendar[:,7]*fCalendar[:,6])*priceA

                        elif thisService == 1:
                                sType='B'
                                priceList = list()
                                priceB=random.uniform(meanPriceB-intervalleB/2,meanPriceB+intervalleB/2)
                                for row in fCalendar:

                                        if row[3]==15:
                                                priceList.append(row[6]*maxPriceB)
                                        else:
                                                priceList.append(row[6]*priceB)
                                price=np.array(priceList)
                                price = fCalendar[:,7]*price


                        elif thisService == 2:
                                sType='C'
                                priceList = list()
                                priceC=random.uniform(meanPriceC-intervalleC/2,meanPriceC+intervalleC/2)
                                priceMinC=random.uniform(minPriceC-intervalleC/2,minPriceC+intervalleC/2)

                                for row in fCalendar:
                                        if row[5]==5 or row[5]==6 :
                                                priceList.append(row[6]*priceMinC)
                                        else:
                                                priceList.append(row[6]*priceC)
                                price=np.array(priceList)
                                price = fCalendar[:,7]*price

                        elif thisService == 3:
                                sType='D'
                                priceList = list()
                                priceD=random.uniform(maxPriceD-intervalleD/2,maxPriceD+intervalleD/2)
                                priceMinD=random.uniform(minPriceD-intervalleD/2,minPriceD+intervalleD/2)
                                for row in fCalendar:
                                        randomChoice=random.choice([True, False])
                                        if randomChoice:
                                                priceList.append(row[6]*priceD)
                                        else:
                                                priceList.append(row[6]*priceMinD)
                                price=np.array(priceList)
                                price = fCalendar[:,7]*price

                        elif thisService == 4:
                                sType='E'
                                priceList = list()
                                numMonthStored=monthToDateInput
                                randomNumber=random.choice([True, False])
                                priceE=random.uniform(meanPriceE-intervalleE/2,meanPriceE+intervalleE/2)
                                #print('start')
                                for row in fCalendar:
                                        #print(row[2])
                                        if row[2]==numMonthStored:
                                                if randomNumber:
                                                        priceList.append(row[6]*priceE)
                                                else:
                                                        priceList.append(0)
                                        else:
                                                randomNumber=random.choice([True, False])
                                                numMonthStored=row[2]
                                                if randomNumber:
                                                        priceList.append(row[6]*priceE)
                                                else:
                                                        priceList.append(0)

                                price=np.array(priceList)



                        #thisService = thisUnit.create_group("Service"+sType)
                        #CostCalendrier = thisService.create_dataset("CalendrierBis",data=fCalendar)

                        tableServiceInstance=np.append(np.transpose([np.ones(len(fCalendar))*iDServiceInstance]),fCalendar,axis=1)
                        tableService=np.append(tableServiceInstance,np.transpose([np.ones(len(fCalendar))*thisService]),axis=1)
                        tablePrice=np.append(tableService,np.transpose([np.ones(len(tableService))*price]),axis=1)

                        if GlobalCostMatrix.ndim==0:
                                GlobalCostMatrix=copy.copy(tablePrice)
                        else:
                                GlobalCostMatrix=np.append(GlobalCostMatrix,tablePrice,axis=0)


        #idDays=np.arange(len(GlobalCostMatrix))
        #GlobalCostMatrix=np.append(np.transpose([idDays]),GlobalCostMatrix,axis=1)
        #print(GlobalCostMatrix[len(GlobalCostMatrix)-1])

        sNameGlobal=np.append(['ID_Instance'],sNameGlobal)
        #print(sNameGlobal)

        GlobalDataFrame=pd.DataFrame(data=GlobalCostMatrix,columns=sNameGlobal)


        return GlobalDataFrame








