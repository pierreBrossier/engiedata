#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 10:15:15 2021

@author: pierre
"""

# %%
# Change datetime format into float columns
# ----------------
def date_to_int(spark,input_spark_df):
    
    from pyspark.sql import functions as F
    
    output_spark_df=input_spark_df.withColumn('Year',F.year('Date'))
    output_spark_df=output_spark_df.withColumn('MonthOfYear',F.month('Date'))
    output_spark_df=output_spark_df.withColumn('WeekOfYear',F.weekofyear('Date'))
    output_spark_df=output_spark_df.withColumn('DayOfYear',F.dayofyear('Date'))
    output_spark_df=output_spark_df.withColumn('DayOfMonth',F.dayofmonth('Date'))
    output_spark_df=output_spark_df.withColumn('DayOfWeek',F.dayofweek('Date'))
     
    print('Il y a '+str(output_spark_df.count())+' après date_to_int')  
        
    return output_spark_df

# %%
# Change datetime format into float columns
# ----------------
def transform_null_values(spark,input_spark_df):
    
    from pyspark.sql import functions as F
    
    
    input_spark_df.printSchema()
    
    print('les valeurs null')
    input_spark_df.select([F.count(F.when(F.col(c).isNull(), c)).alias(c) for c in input_spark_df.columns]).show()
     
    print('les valeurs not-a-number')
    input_spark_df.select([F.count(F.when(F.isnan(c), c)).alias(c) for c in ['Amount']]).show()
    
    print('les valeurs vide')
    input_spark_df.select([F.count(F.when((F.col(c).contains('None')) | 
                                          (F.col(c)==''), c)).alias(c) for c in input_spark_df.columns]).show()
    
    input_spark_df.filter(F.isnan(F.col('Amount'))).show()
    print(input_spark_df.filter(F.isnan(F.col('Amount'))).count())
    
    input_spark_df.filter(F.col('Service').isNull()).show()
    print(input_spark_df.filter(F.col('Service').isNull()).count())
    
    input_spark_df.filter(F.col('Date').isNull()).show()
    print(input_spark_df.filter(F.col('Date').isNull()).count())
    
    
    output_spark_df=input_spark_df.fillna(value=0,subset=["Amount"])
    output_spark_df.printSchema()
    
    #output_spark_df=input_spark_df.dropna(subset=["Amount"])
    output_spark_df=output_spark_df.dropna(subset=["Service"])
    output_spark_df=output_spark_df.dropna(subset=["Account"])
    output_spark_df=output_spark_df.dropna(subset=["Date"])
    
    
    #df.na.fill({'Height': '10', 'Name': 'Bob'})
    
    output_spark_df.printSchema()
    
        
    print('Il y a '+str(output_spark_df.count())+' après transform_null_values')
    
    return output_spark_df

# %%
# Change datetime format into float columns
# ----------------
def string_to_date(spark,input_spark_df):
    
    from pyspark.sql import functions as F
    
    output_spark_df=input_spark_df.drop('Period End') 
    output_spark_df=output_spark_df.drop('CSP')
    
    output_spark_df=output_spark_df.withColumn('Period Start',F.to_date('Period Start', 'yyyy-MM-dd'))
    
    output_spark_df=output_spark_df.withColumn('Date',F.col('Period Start'))
    output_spark_df=output_spark_df.drop('Period Start') 

    print('Il y a '+str(output_spark_df.count())+' après string_to_date')
    return output_spark_df



# %%
# Evaluation du nobres de jours manquants et consolidation des colonnes
# ----------------
def evaluate_empty_date(spark,input_spark_df):

    from pyspark.sql import functions as F
    import numpy as np
    
    conversion_table=input_spark_df.select("Account","Service")

    conversion_table=input_spark_df.groupBy("Account","Service").count().alias('NumberOfDays')
    
    conversion_table=conversion_table.select(F.col("count").alias("NumberOfDays"))
    
    #conversion_table.show()  
    
    #conversion_table.select('count').show(conversion_table.count(),False)
    
    repartition_table=conversion_table.groupBy("NumberOfDays").count()
    
    repartition_table=repartition_table.select("NumberOfDays",F.col("count").alias('NumberOfInstances'))
    
    repartition_table=repartition_table.sort("NumberOfDays")
    
    #repartition_table.show(repartition_table.count(),False)
    
    A=np.array(repartition_table.select('NumberOfInstances').collect())
    B=np.array(repartition_table.select('NumberOfDays').collect())
    
    print('il manque '+str(np.sum((B[-1]-B)*A))+' jours sur '+str(input_spark_df.count()))
    
    


# %%
# Add ID columns to the Dataframe
# ----------------
def set_IDs_Instance(spark,input_spark_df):
    
    from pyspark.sql import functions as F
    from pyspark.sql import window
    
    windowSpec  = window.Window.orderBy("Service","Account")
    output_spark_df = input_spark_df.withColumn("ID_Instance",F.dense_rank().over(windowSpec)) 
    
    #output_spark_df.select('ID_Instance','Account','Service').dropDuplicates(['ID_Instance']).show()    
    
    windowSpec2  = window.Window.orderBy("Account")
    output_spark_df=output_spark_df.withColumn("ID_Account",F.dense_rank().over(windowSpec2)) 
    
    windowSpec3 = window.Window.orderBy("Service")
    output_spark_df=output_spark_df.withColumn("ID_Service",F.dense_rank().over(windowSpec3)) 
    
    #output_spark_df.select('ID_Instance','ID_Account','Account','ID_Service','Service').dropDuplicates(['ID_Instance']).show()    
    
    instance_table=output_spark_df.select('ID_Instance','ID_Account','Account','ID_Service','Service').dropDuplicates(['ID_Instance'])
    
    output_spark_df=output_spark_df.drop('Account')
    output_spark_df=output_spark_df.drop('Service')

    return output_spark_df,instance_table

# %%
# Add ID columns to the Dataframe
# ----------------
def set_Day_ID(spark,input_spark_df):
    
    from pyspark.sql import functions as F
    from pyspark.sql import window
    
    
    #windowSpec  = window.Window.orderBy("Year","DayOfYear")
    windowSpec  = window.Window.orderBy("Date")
    output_spark_df = input_spark_df.withColumn("ID_Day",F.dense_rank().over(windowSpec)) 

    day_table=output_spark_df.select("ID_Day","Date").distinct()
    #day_table=output_spark_df.select("ID_Day","Date","Year","DayOfYear","MonthOfYear","DayOfMonth","WeekOfYear","DayOfWeek").distinct()
    #output_spark_df.sample(0.02).show(100)
                  
    
    
    return output_spark_df,day_table


# %%
# Complete missing values of days
# ----------------
def set_empty_date(spark,input_spark_df):
    
    from pyspark.sql import functions as F
    from datetime import timedelta
    from itertools import product
    
    #input_spark_df.orderBy("ID_Instance").show(50)
    
    min_max_date = input_spark_df.agg(F.min(input_spark_df.Date), F.max(input_spark_df.Date)).head()
    
    first_date, last_date = [ts for ts in min_max_date]
      
    all_days_in_range = [first_date + timedelta(days=d) for d in range((last_date - first_date).days + 1)]
    
    #print(all_days_in_range)
    
    select_df= input_spark_df.select("ID_Instance",'ID_Account','Id_Service')
    select_df=select_df.dropDuplicates(['ID_Instance'])

    Instances = [row.ID_Instance for row in input_spark_df.select("ID_Instance").distinct().collect()]
    
    #print(Instances)

    dates_by_Instances = spark.createDataFrame(product(Instances, all_days_in_range),
                                        schema=("new_ID_Instance", "new_Date"))
    
    
    dates_by_Instances.show()
    
    print('Il y a '+str(dates_by_Instances.count())+' date_by_instances')
    
    #dates_by_Instances.orderBy("new_ID_Instance").show(50)
    
    df2 = (dates_by_Instances.join(input_spark_df.select("ID_Instance","Date","Amount"),
                            (input_spark_df.Date == dates_by_Instances.new_Date)
                            & (dates_by_Instances.new_ID_Instance == input_spark_df.ID_Instance),
                            how="left")
           .drop(input_spark_df.ID_Instance)\
           .drop(input_spark_df.Date)
       )

    df2=df2.withColumnRenamed("new_ID_Instance",'ID_Instance')
    df2=df2.withColumnRenamed("new_Date",'Date')
    
    '''
    print('df2')
    df2.sort('ID_Instance').dropDuplicates(['ID_Instance']).show()    
    '''
    df3 = df2.join(select_df,'ID_Instance','outer')
    '''
    print('df3')
    df3.sort('ID_Instance').dropDuplicates(['ID_Instance']).show()    
    '''
    df4=df3.na.fill(value=0)

    return df4

# %%
# Transforme negative value to -1
# ----------------
def set_neg_values(spark,input_spark_df):
    
    from pyspark.sql import functions as F

    #correctNegativeDiff = F.udf(lambda diff: -1 if diff < 0 else diff)
    
    output_spark_df = (input_spark_df.withColumn("new_Amount",F.when(F.col('Amount')<0,-1).otherwise(F.col('Amount')))
                       .drop('Amount')
                       .withColumnRenamed('new_Amount','Amount'))
    
    '''
    output_spark_df = (input_spark_df.withColumn("new_Amount",correctNegativeDiff(input_spark_df.Amount))
                       .drop('Amount')
                       .withColumnRenamed('new_Amount','Amount'))
    '''
    return output_spark_df

# %%
# Transforme negative value to -1
# ----------------
def plot_normal_distrib(input_spark_df):
    
    import numpy as np
    import matplotlib.pyplot as plt
    
    def normal_dist(x , mean , sd):
        prob_density = (np.pi*sd) * np.exp(-0.5*((x-mean)/sd)**2)
        return prob_density 
    
    #Calculate mean and Standard deviation.
    x=np.array(input_spark_df.select('Amount').sort(['Amount']).collect())
    mean = np.mean(x)
    sd = np.std(x)
    
    
    #Apply function to the data.
    pdf = normal_dist(x,mean,sd)
    
    #Plotting the Results
    plt.plot(x,pdf , color = 'red',marker = '.' ,linestyle=None)
    plt.xlabel('Amount')
    plt.ylabel('Probability Density')
    plt.show()
    
# %%
# Transforme max outlier
# ----------------
def winsorize_values(spark,input_spark_df):
    
    from pyspark.sql import functions as F

    #correctNegativeDiff = F.udf(lambda diff: -1 if diff < 0 else diff)
    
    quantile_values=input_spark_df.approxQuantile(['Amount'],[0.05,0.999],0)[0]
    
    print(quantile_values[1])
    output_spark_df = (input_spark_df.withColumn('new_Amount', \
                                                (F.when(input_spark_df.Amount > quantile_values[1], quantile_values[1])\
                                                          .otherwise(input_spark_df.Amount)))
                       .drop('Amount')
                       .withColumnRenamed('new_Amount','Amount'))
          
                       #.drop('Amount')
                       #.withColumnRenamed('new_Amount','Amount'))
    
    '''
    output_spark_df = (input_spark_df.withColumn("new_Amount",correctNegativeDiff(input_spark_df.Amount))
                       .drop('Amount')
                       .withColumnRenamed('new_Amount','Amount'))
    '''
    
    return output_spark_df

# %%
# Add a Period column
# ----------------
def get_period_table(spark,input_spark_df,period,step):
    
    from pyspark.sql import functions as F
    from datetime import timedelta
    import numpy as np
    import pandas as pd
    from itertools import product
    
    min_max_date = input_spark_df.agg(F.min(input_spark_df.Date), F.max(input_spark_df.Date)).head()
        
    first_date, last_date = [ts for ts in min_max_date]
    count=(last_date - first_date).days + 1

    previous_first_date=first_date
    list_period=list(range(period))
    list_ID=list(np.ones(period))
                    
    for iPeriod in range((count-period)//step):
        list_ID=np.concatenate((list_ID,list((iPeriod+2)*np.ones(period))))
        first_date_period=previous_first_date + timedelta(days=step)
        last_date_period=first_date_period+ timedelta(days=period)
        period_range=list(range((first_date_period - first_date).days,(last_date_period - first_date).days))
        list_period=np.concatenate((list_period,period_range))
        previous_first_date=first_date_period
        
    list_period=list_period.astype(float)
    list_ID=list_ID.astype(int)

    all_periods_in_range = [first_date + timedelta(days=d) for d in list_period]
    
    data=[all_periods_in_range,list_ID]
    
    #print(data)
    
    #print(len(all_periods_in_range))
    #print(len(list_ID))
    
    data=pd.DataFrame(data=np.transpose(data))
    data=data.reset_index()
    #print(data)
    
    #print(data)
    period_dates_table = spark.createDataFrame(data).toDF('index_Period','Dates_of_Period','ID_Period')
    
    Instances = [row.ID_Instance for row in input_spark_df.select("ID_Instance").distinct().collect()]
    
    indexPeriod=(data['index'].values).astype(int)
    indexPeriod=indexPeriod.tolist()
    
    print(type(indexPeriod))
    print(type(Instances))
    period_by_Instances = spark.createDataFrame(product(Instances, indexPeriod),schema=['new_ID_Instance','index_Period'])
    
    period_by_Instances=(period_by_Instances.join(period_dates_table,period_by_Instances.index_Period == period_dates_table.index_Period,'outer')
                         .drop('index_Period'))
    #period_by_Instances.sort('new_ID_Instance','Dates_of_Period').show(100)
    
    return period_by_Instances
    
# %%
# Transforme calendarCost to signature_df
# ----------------
def transform_to_signatures(spark,calendarCost_df,period_by_Instances):
    
    from pyspark.sql import functions as F
    from pyspark.sql import window
    import math
    from pyspark.sql.types import FloatType,DoubleType
    #from datetime import timedelta
    
    full_df=(period_by_Instances.join(calendarCost_df,
        (period_by_Instances.new_ID_Instance == calendarCost_df.ID_Instance) &
        (period_by_Instances.Dates_of_Period == calendarCost_df.Date),'left')
                         .drop('new_ID_Instance')
                         .drop('Dates_of_Period'))

    
    #signature_df=(full_df.groupBy('ID_Instance','ID_Period').avg("Amount").alias("sum"))
    
    # ----------------
    ## Any day
    signature_df=(full_df.groupby('ID_Instance','ID_Period').agg(
        F.mean('Amount').alias('mean'), 
        F.stddev('Amount').alias('std'),
        F.min('Amount').alias('min'), 
        F.expr('percentile(Amount, array(0.25))')[0].alias('%25'), 
        F.expr('percentile(Amount, array(0.5))')[0].alias('%50'), 
        F.expr('percentile(Amount, array(0.75))')[0].alias('%75'), 
        F.max('Amount').alias('max')))
    
    #signature_df.sort('ID_Instance','ID_Period').show()
    print('signature_df -> jours : '+str(signature_df.count()))
    
    ## per day of the week
    # ----------------
    for iDay in range(7):
        string=str(iDay+1)+'_Day'
        thisDay_df=(full_df.groupBy('ID_Instance','ID_Period','DayOfWeek').agg(
                                                               F.mean("Amount").alias(string))
                                                               .filter(full_df.DayOfWeek==iDay+1)
                                                               )
        #print('this day df '+str(thisDay_df.count()))
        #thisDay_df.sort('ID_Instance','ID_Period').show()  
        thisDay_df=thisDay_df.withColumnRenamed('ID_Instance','new_ID_Instance')
        thisDay_df=thisDay_df.withColumnRenamed('ID_Period','new_ID_Period')
        
                                                       
        signature_df=(signature_df.join(thisDay_df,
                                       (signature_df.ID_Instance == thisDay_df.new_ID_Instance) &
                                       (signature_df.ID_Period == thisDay_df.new_ID_Period),
                                       'inner')
         .drop('new_ID_Instance')
         .drop('new_ID_Period')
         .drop('DayOfWeek'))
    
        #signature_df=signature_df.withColumn(str(iDay)+'_Day',full_df.select('Amount',F.when(full_df.DayOfWeek==iDay,full_df.Amount)))
        
    #signature_df.sort('ID_Instance','ID_Period').show()
    #signature_df.show()
    print('signature  df -> jours semaine : '+str(signature_df.count()))
    
    ## per day of the period
    # ----------------
    
    my_window = window.Window.partitionBy().orderBy("ID_Instance","ID_Period","Date")
    full_df = full_df.withColumn("prevAmount", F.when((F.lag(full_df.ID_Instance).over(my_window) != full_df.ID_Instance) |
                                 (F.lag(full_df.ID_Period).over(my_window) != full_df.ID_Period),None)
                                 .otherwise(F.lag(full_df.Amount).over(my_window)))
        
            
    full_df = full_df.withColumn("Diff", F.when(F.isnull(full_df.Amount - full_df.prevAmount), 0)
                              .otherwise(full_df.Amount - full_df.prevAmount))
    
    #full_df.sort('ID_Instance','ID_Period','Date').filter(F.col('ID_Instance')>2).show(300)
    
    thisDiff_df=(full_df.groupby('ID_Instance','ID_Period').agg(
        F.mean('Diff').alias('meanDiff'), 
        F.max('Diff').alias('maxDiff'),
        F.min('Diff').alias('minDiff')))

    #thisDiff_df.show()
    
    thisDiff_df=thisDiff_df.withColumnRenamed('ID_Instance','new_ID_Instance')
    thisDiff_df=thisDiff_df.withColumnRenamed('ID_Period','new_ID_Period')
     
    signature_df=(signature_df.join(thisDiff_df,
                                         (signature_df.ID_Instance == thisDiff_df.new_ID_Instance) &
                                       (signature_df.ID_Period == thisDiff_df.new_ID_Period),
                                       'inner')
             .drop('new_ID_Instance')
             .drop('new_ID_Period'))
    
    #thisMonth_df=(full_df.groupBy('ID_Instance','ID_Period','DayOfMonth').agg(
                                                              # F.mean("Amount")))
                               
    
    #full_df.orderBy("ID_Instance","ID_Period").filter(F.col('ID_Instance')>2).show(200)

    max_df=full_df.groupBy("ID_Instance","ID_Period").agg(F.max('Amount').alias('max_Amount'))
    
    max_df=(max_df.withColumnRenamed("ID_Instance","max_ID_Instance")
            .withColumnRenamed("ID_Period","max_ID_Period"))
                                                   
    #max_df.orderBy("ID_Instance","ID_Period").filter(F.col('ID_Instance')>2).show()

    position_df=full_df.select("ID_Instance","ID_Period","Amount","DayOfMonth").join(max_df,(full_df.Amount == max_df.max_Amount) &
                             (full_df.ID_Period == max_df.max_ID_Period) &
                             (full_df.ID_Instance == max_df.max_ID_Instance)
                             ,how='leftsemi')
    
    #position_df.orderBy("ID_Instance","ID_Period").filter(F.col('ID_Instance')>2).show()
    
    unique_df=position_df.groupBy("ID_Instance","ID_Period").count()
    
    unique_df.printSchema()
    
    unique_df=unique_df.withColumn('uniqueMax',F.when(F.col('count') > 1, 0).otherwise(F.col('count')))
    unique_df=unique_df.drop("count")
    
    unique_df=unique_df.filter(F.col('uniqueMax')== 1)
    
    position_df=position_df.join(unique_df,(position_df.ID_Period == unique_df.ID_Period) &
                             (position_df.ID_Instance == unique_df.ID_Instance)
                             ,how='leftsemi')
    
    #unique_df.orderBy("ID_Instance","ID_Period").show()
    
    #position_df=position_df.join(unique_df,
    
    #position_df.orderBy("ID_Instance","ID_Period").filter(F.col('ID_Instance')>0).show()
    
    position_df=position_df.withColumnRenamed('ID_Instance','new_ID_Instance')
    position_df=position_df.withColumnRenamed('ID_Period','new_ID_Period')
    position_df=position_df.withColumnRenamed('DayOfMonth','max_DayOfMonth')
    position_df=position_df.drop('Amount')
    
    print('position_df : '+str(position_df.count()))
    
    signature_df=(signature_df.join(position_df,
                                         (signature_df.ID_Instance == position_df.new_ID_Instance) &
                                       (signature_df.ID_Period == position_df.new_ID_Period),
                                       'left')
                  .drop('new_ID_Instance')
                  .drop('new_ID_Period'))
    
    print('signature  df -> jours mois : '+str(signature_df.count()))
     
    def cos_sim(vec):
        return math.cos(2*math.pi*vec/31)
    
    cos_sim_udf = F.udf(cos_sim, DoubleType())
   
    def sin_sim(vec):
        return math.sin(2*math.pi*vec/31)
    
    sin_sim_udf = F.udf(sin_sim, DoubleType())
     
    signature_df=signature_df.na.fill(value=0,subset=["max_DayOfMonth"])
    signature_df=signature_df.withColumn("cos_max_DayOfMonth",cos_sim_udf('max_DayOfMonth'))
    signature_df=signature_df.withColumn("sin_max_DayOfMonth",sin_sim_udf('max_DayOfMonth'))
    signature_df=signature_df.drop("max_DayOfMonth")
    #signature_df=signature_df.withColumn("sin_max_DayOfMonth",math.sin(2*math.pi*F.col('max_DayOfMonth')/31))
    
    '''
    print('Il y a '+str(signature_df.count())+' avant transform_to_signatures.dropna')  
    signature_df=signature_df.dropna()
    print('Il y a '+str(signature_df.count())+' apres transform_to_signatures.dropna') 
    def set_df_columns_nullable(spark, df, column_list, nullable=True):
        for struct_field in df.schema:
            if struct_field.name in column_list:
                struct_field.nullable = nullable
        df_mod = spark.createDataFrame(df.rdd, df.schema)
        return df_mod
    signature_df=set_df_columns_nullable(spark,signature_df,signature_df.columns,nullable=False)
    '''
    
    signature_df.printSchema()
    
    def set_df_columns_float(spark, input_df):
        output_df=input_df
        for c in input_df.columns:
            output_df= output_df.select(c).dropna()
            output_df= output_df.select(c).cast('float')
        return output_df
    
    #signature_df=set_df_columns_float(spark,signature_df)

    #signature_df=signature_df.cast('float')

    signature_df.printSchema()

    return signature_df


# %%
# get features_df
# ----------------
def get_features_df(spark,signature_df):
    
   

    signature_df=signature_df.orderBy("ID_Instance","ID_Period")
    pure_signature_df=signature_df.drop('ID_Instance').drop('ID_Period')
    #pure_signature_df.show()
    
    import pyspark.sql.functions as F
    
    pure_signature_df.printSchema()
    
    '''
    for c in pure_signature_df.columns:
        signature_df.select(c).where(F.isnan(c)).show() 
                       
    Dict_Null = {col:pure_signature_df.filter(pure_signature_df[col].isNull()).count() for col in pure_signature_df.columns}
    print(Dict_Null)
     '''
     
    '''
    columns = pure_signature_df.columns    
    
    features_df = pure_signature_df.withColumn("features", F.array(columns)).select("features")

    features_df.show()
    
    features_df.printSchema()
    '''
    from pyspark.ml.feature import VectorAssembler
    
    vectorAssembler = VectorAssembler(inputCols = pure_signature_df.columns, outputCol = "features")
    features_df = vectorAssembler.transform(pure_signature_df)
    features_df = features_df.select(['features'])

    features_df.printSchema()
    
    return features_df

# %%
# train K-means
# ----------------
def train_K_means(spark,input_spark_df):
    
    import pyspark.sql.functions as F
    from pyspark.ml.clustering import KMeans
    from pyspark.ml.evaluation import ClusteringEvaluator
    from pyspark.sql.types import DoubleType,ArrayType
    
   

    # Trains a k-means model.
    kmeans = KMeans().setK(2).setSeed(1)
    model = kmeans.fit(input_spark_df)
    
    print("train")
    # Make predictions
    predictions = model.transform(input_spark_df)
    print("pred")
    '''
    # Evaluate clustering by computing Silhouette score
    evaluator = ClusteringEvaluator()
    
    silhouette = evaluator.evaluate(predictions)
    print("Silhouette with squared euclidean distance = " + str(silhouette))
    '''
    # Shows the result.
    centers = model.clusterCenters()
    print("Cluster Centers: ")
    for center in centers:
        print(center)

    
    return predictions
