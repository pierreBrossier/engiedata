"""
===========================================================
 Showing learning curves for a pair ofdata:
        - (Y_train_score) versus size of x Train
        - (Y_test  score) versus size of x Train
===========================================================
Pierre Brossier
date : 14-05-2021

This fonction loadthe data and apply EngieAnalys n tilmes to show a figure of the n dots caracterizing the learning curves


=========== ========================================================
Access to files :
"""
print(__doc__)
# Re définition du chemin d'accès root
# ----------------------
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
print('\n')


# %
# Load the 3rd parties libraries
# ----------------
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy import stats

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.metrics import roc_curve

from sklearn.model_selection import train_test_split

# Created modules and import csv
# ----------------

from EngieDataGenerator import getEngieDataSet
from EngieDataCollection import getDataSet
from EngieDataCollection import get_X_y
# ----------------------


# %%
# Creating n times algorythme
# ----------------

n_curves = 10
n_start = 2
n_end = 100

# ----------------
n_step=(n_end-n_start)/n_curves

X_train_size=list()
Y_train_score=list()
Y_test_score=list()

for idn in range(n_curves):

    this_n=round(n_start+idn*n_step)

    df = getEngieDataSet(this_n,2021,11)
    ds = getDataSet(df)

    ds = ds.sample(n = len(ds))
    ds_train, ds_test = train_test_split(ds, test_size=0.2)

    X_train, y_train =get_X_y(ds_train)
    X_train_size.append(len(X_train))

    X_test, y_test =get_X_y(ds_test)

    rf=RandomForestClassifier(bootstrap=True, random_state = 1)
    rf.fit(X_train, y_train)

    Y_train_score.append(rf.score(X_train,y_train))

    Y_test_score.append(rf.score(X_test,y_test))


# %%
# PLot
# ----------------


def get_learning_curves(X_train_size,Y_train_score,Y_test_score):

    plt.plot(X_train_size,Y_train_score,'-bo',label='Training Score')
    plt.plot(X_train_size,Y_test_score,'-ro',label='Testing Score')

    plt.xlabel('number of train data')
    plt.ylabel('score %')
    plt.title('Learning Curves')
    plt.legend()
    #plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
    #plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
    plt.show()


get_learning_curves(X_train_size,Y_train_score,Y_test_score)

