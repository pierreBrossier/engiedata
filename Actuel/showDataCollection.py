"""
===========================================================
 Show Data Collection - 2 graph and reporting
===========================================================
copy by Pierre Brossier
date : 17-05-2021

=========== ========================================================
"""
print(__doc__)

# Re définition du chemin d'accès root
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
# ----------------------


# %%
# Load the 3rd parties libraries and created modules and import csv
# ----------------
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy import stats

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.metrics import roc_curve

from sklearn.model_selection import train_test_split

from EngieDataGenerator import getEngieDataSet



# %%s
# Show the dispersion in a subplots per data series types
# ----------------
def show_dataSet_rawCollection(df):

 zLim=max(df['Price'])

 # affichage 3D du Prix-DayInWeek-DayInMonth
 # set up a figure twice as wide as it is tall


 fig,axs = plt.subplots(3,6,figsize=[2*6.4, 2*4.8])


 axs[0,0]=plt.subplot(2,3,1, projection='3d')
 axs[0,1]=plt.subplot(2,3,2, projection='3d')
 axs[0,2]=plt.subplot(2,3,3, projection='3d')
 axs[1,0]=plt.subplot(2,3,4, projection='3d')
 axs[1,1]=plt.subplot(2,3,5, projection='3d')
 axs[1,2]=plt.subplot(2,3,6, projection='3d')

 #===============
 #  First subplot
 #===============
 # set up the axes for the first plot
 #axs[0] = fig.add_subplot(, 1, projection='3d')

 # plot a 3D surface like in the example mplot3d/surface3d_demo
 zdataA =df[df['Service'] ==0]['Price']
 xdataA = df[df['Service'] ==0]['DayInWeek']
 ydataA = df[df['Service'] ==0]['DayInMonth']

 scatterA = axs[0,0].scatter3D(xdataA, ydataA, zdataA, c=(zdataA),cmap='viridis', linewidth=0.5)

 axs[0,0].set_zlim(0, zLim)
 axs[0,0].set_title('serie A')

 #===============
 # Second subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 2, projection='3d')
 # set up the axes for the second plot
 zdataB=df[df['Service'] ==1]['Price']
 xdataB = df[df['Service'] ==1]['DayInWeek']
 ydataB = df[df['Service'] ==1]['DayInMonth']
 scatterB = axs[0,1].scatter3D(xdataB, ydataB, zdataB, c=(zdataB),cmap='plasma', linewidth=0.5)
 axs[0,1].set_zlim(0, zLim)
 axs[0,1].set_title('serie B')


 #===============
 #  Third subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 3, projection='3d')

 # plot a 3D surface like in the example mplot3d/surface3d_demo
 zdataC =df[df['Service'] ==2]['Price']
 xdataC = df[df['Service'] ==2]['DayInWeek']
 ydataC = df[df['Service'] ==2]['DayInMonth']
 scatterC = axs[0,2].scatter3D(xdataC, ydataC, zdataC, c=(zdataC),cmap='magma', linewidth=0.5)
 axs[0,2].set_zlim(0, zLim)
 axs[0,2].set_title('serie C')
 def calc_specificity(y_actual, y_pred, thresh):
  # calculates specificity
  return sum((y_pred < thresh) & (y_actual == 0)) /sum(y_actual ==0)


 #===============
 # Fourth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 4, projection='3d')
 # set up the axes for the second plot
 zdataD=df[df['Service'] ==3]['Price']
 xdataD = df[df['Service'] ==3]['DayInWeek']
 ydataD = df[df['Service'] ==3]['DayInMonth']
 scatterD = axs[1,0].scatter3D(xdataD, ydataD, zdataD, c=(zdataD),cmap='cividis', linewidth=0.5)
 axs[1,0].set_zlim(0, zLim)
 axs[1,0].set_title('serie D')


 #===============
 # Fiveth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 5, projection='3d')
 # set up the axes for the second plot
 zdataE=df[df['Service'] ==4]['Price']
 xdataE = df[df['Service'] ==4]['DayInWeek']
 ydataE = df[df['Service'] ==4]['DayInMonth']
 scatterE = axs[1,1].scatter3D(xdataE, ydataE, zdataE, c=(zdataE),cmap='summer', linewidth=0.5)
 axs[1,1].set_zlim(0, zLim)
 axs[1,1].set_title('serie E')

 #===============
 # Sixth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 6, projection='3d')
 # set up the axes for the second plot
 zdata=df['Price']
 xdata = df['DayInWeek']
 ydata = df['DayInMonth']
 scatter = axs[1,2].scatter3D(xdata, ydata, zdata, c=(zdata),cmap='gist_gray', linewidth=0.5)
 axs[1,2].set_zlim(0, zLim)
 axs[1,2].set_title('All series')

 plt.show()


# %%s
# For each load cases show the signature
# ----------------
# affichage 3D du Prix-DayInWeek-DayInMonth
# set up a figure twice as wide as it is tall

def show_dataSet_signature(ds):

 zLim=max(ds.max(axis = 1, skipna = True))

 fig2,axs = plt.subplots(3,6,figsize=[2*6.4, 2*4.8])

 axs[0,0]=plt.subplot(2,3,1, projection='3d')
 axs[0,1]=plt.subplot(2,3,2, projection='3d')
 axs[0,2]=plt.subplot(2,3,3, projection='3d')
 axs[1,0]=plt.subplot(2,3,4, projection='3d')
 axs[1,1]=plt.subplot(2,3,5, projection='3d')
 axs[1,2]=plt.subplot(2,3,6, projection='3d')

 #===============
 #  First subplot
 #===============
 # set up the axes for the first plot

 #axs[0] = fig.add_subplot(, 1, projection='3d')

 # plot a 3D surface like in the example mplot3d/surface3d_demo

 dsA=ds[ds['Service'] ==0]
 if not dsA.empty:
  zdataA=np.empty([len(dsA),len(dsA.columns)-2])
  idy=0
  for idx,columns in enumerate(dsA):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataA[:,idy] =dsA[columns]
    idy=idy+1

  ny, nx = zdataA.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterA = axs[0,0].plot_surface(xv,yv,zdataA,rstride=1, cstride=1,
                  cmap='viridis', edgecolor='none')

  axs[0,0].set_xlabel("Signatures")
  axs[0,0].axes.xaxis.set_ticks([])
  axs[0,0].set_ylabel("jeu d'instance")
  axs[0,0].axes.yaxis.set_ticks([])
  axs[0,0].set_zlim(0, zLim)
  axs[0,0].set_title('serie A')

 #===============
 # Second subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 2, projection='3d')
 # set up the axes for the second plot
 dsB=ds[ds['Service'] ==1]
 if not dsB.empty:
  zdataB=np.empty([len(dsB),len(dsB.columns)-2])
  idy=0
  for idx,columns in enumerate(dsB):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataB[:,idy] =dsB[columns]
    idy=idy+1

  ny, nx = zdataB.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterB = axs[0,1].plot_surface(xv,yv,zdataB,rstride=1, cstride=1,
                  cmap='plasma', edgecolor='none')
  axs[0,1].set_zlim(0, zLim)
  axs[0,1].set_title('serie B')
  axs[0,1].set_xlabel("Signatures")
  axs[0,1].axes.xaxis.set_ticks([])
  axs[0,1].set_ylabel("jeu d'instance")
  axs[0,1].axes.yaxis.set_ticks([])
  axs[0,1].set_zlim(0, zLim)
  axs[0,1].set_title('serie B')

 #===============
 #  Third subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 3, projection='3d')

 # plot a 3D surface like in the example mplot3d/surface3d_demo
 dsC=ds[ds['Service'] ==2]
 if not dsC.empty:
  zdataC=np.empty([len(dsC),len(dsC.columns)-2])
  idy=0
  for idx,columns in enumerate(dsC):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataC[:,idy] =dsC[columns]
    idy=idy+1

  ny, nx = zdataC.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterC = axs[0,2].plot_surface(xv,yv,zdataC,rstride=1, cstride=1,
                  cmap='magma', edgecolor='none')
  axs[0,2].set_xlabel("Signatures")
  axs[0,2].axes.xaxis.set_ticks([])
  axs[0,2].set_ylabel("jeu d'instance")
  axs[0,2].axes.yaxis.set_ticks([])
  axs[0,2].set_zlim(0, zLim)
  axs[0,2].set_title('serie C')
 #===============
 # Fourth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 4, projection='3d')
 # set up the axes for the second plot
 dsD=ds[ds['Service'] ==3]
 if not dsD.empty:
  zdataD=np.empty([len(dsD),len(dsD.columns)-2])
  idy=0
  for idx,columns in enumerate(dsD):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataD[:,idy] =dsD[columns]
    idy=idy+1

  ny, nx = zdataD.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterD = axs[1,0].plot_surface(xv,yv,zdataD,rstride=1, cstride=1,
                  cmap='summer', edgecolor='none')
  axs[1,0].set_xlabel("Signatures")
  axs[1,0].axes.xaxis.set_ticks([])
  axs[1,0].set_ylabel("jeu d'instance")
  axs[1,0].axes.yaxis.set_ticks([])
  axs[1,0].set_zlim(0, zLim)
  axs[1,0].set_title('serie D')

 #===============
 # Fiveth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 5, projection='3d')
 # set up the axes for the second plot
 dsE=ds[ds['Service'] ==4]
 if not dsE.empty:
  zdataE=np.empty([len(dsE),len(dsE.columns)-2])
  idy=0
  for idx,columns in enumerate(dsE):
   #print(columns)
   if columns!=('Service','') and columns!=('ID_Instance',''):
    zdataE[:,idy] =dsE[columns]
    idy=idy+1

  ny, nx = zdataE.shape
  x = range(nx)
  y = range(ny)
  xv, yv = np.meshgrid(x, y)

  scatterE = axs[1,1].plot_surface(xv,yv,zdataE,rstride=1, cstride=1,
                  cmap='viridis', edgecolor='none')
  axs[1,1].set_xlabel("Signatures")
  axs[1,1].axes.xaxis.set_ticks([])
  axs[1,1].set_ylabel("jeu d'instance")
  axs[1,1].axes.yaxis.set_ticks([])
  axs[1,1].set_zlim(0, zLim)
  axs[1,1].set_title('serie E')

 #===============
 # Sixth subplot
 #===============
 # set up the axes for the first plot
 #ax = fig.add_subplot(3, 3, 6, projection='3d')
 # set up the axes for the second plot
 zdata=np.empty([len(ds),len(ds.columns)-2])
 idy=0
 for idx,columns in enumerate(ds):
  #print(columns)
  if columns!=('Service','') and columns!=('ID_Instance',''):
   zdata[:,idy] =ds[columns]
   idy=idy+1

 ny, nx = zdata.shape
 x = range(nx)
 y = range(ny)
 xv, yv = np.meshgrid(x, y)

 scatter = axs[1,2].plot_surface(xv,yv,zdata,rstride=1, cstride=1,
                 cmap='gist_gray', edgecolor='none')
 axs[1,2].set_xlabel("Signatures")
 axs[1,2].axes.xaxis.set_ticks([])
 axs[1,2].set_ylabel("jeu d'instance")
 axs[1,2].axes.yaxis.set_ticks([])
 axs[1,2].set_zlim(0, zLim)
 axs[1,2].set_title('All series')


 fig2.show()


# %%
# Access results
# ----------------
def generate_report(rf,x_iput,y_output,stringName):

 y_result = rf.predict(x_iput)

 score = rf.score(x_iput,y_output)

 proba = rf.predict_proba(x_iput)

 worst=np.zeros_like(y_output)
 best=np.zeros_like(y_output)

 for idx,row in enumerate(proba):
   best[idx]=row[int(y_output[idx])]
   row[int(y_output[idx])]=0
   worst[idx]=max(row)
# ----------------

 #edit rapport
 print('Random Forest')
 print('----------------------')
 print('\n')
 print('Reussite '+stringName+' : '+str(score*100)+'%')
 print('\n')
 worstString='\n'

 worstDF=pd.DataFrame(worst)
 print('Mean Worst Guess proba '+stringName+' : '+str(round(worstDF.describe().loc[ 'mean' , : ][0]*100,3))+'%')

 bestDF=pd.DataFrame(best)
 print('Mean Best Guess proba '+stringName+' : '+str(round(bestDF.describe().loc[ 'mean' , : ][0]*100,3))+'%')

