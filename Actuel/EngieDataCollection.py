"""
===========================================================
 Random forest clustering on a medical no show dataset
===========================================================
copy by Pierre Brossier
date : 17-05-2021

dataset from : https://www.kaggle.com/joniarroba/noshowappointments
code from    : https://towardsdatascience.com/machine-learning-with-datetime-feature-engineering-predicting-healthcare-appointment-no-shows-5e4ca3a85f96

In this example we'll try to compute cluster by random forest

=========== ========================================================
"""
print(__doc__)

# Re définition du chemin d'accès root
import os
import inspect
src_file_path = inspect.getfile(lambda: None)
#print(src_file_path)
pathRootFolder=os.path.dirname(src_file_path)
os.chdir(pathRootFolder)
print(os.listdir(pathRootFolder))
# ----------------------


# %%
# Load the 3rd parties libraries and created modules and import csv
# ----------------
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy import stats

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, recall_score
from sklearn.metrics import roc_curve

from sklearn.model_selection import train_test_split

from EngieDataGenerator import getEngieDataSet


# %%s
# Transform the df matrix of days into batches of usecases : a year per service instance
# ----------------
def getDataSet(df):
 # creating headers
 firstInstance=pd.DataFrame(df.iloc[0:400])

 #day in week
 aWeek=firstInstance.groupby(by=['DayInWeek'])
 thisDayDescriptionDf=aWeek.Price.describe()
 # setting index for day
 a=list(range(len(thisDayDescriptionDf)))
 b=[str(a) for a in a]
 day_case_id=["D" + b for b in b]
 thisDayDescriptionDf.insert(0, "Id_case", day_case_id, True)
 thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="count", axis=1)
 thisDayDescriptionDf=thisDayDescriptionDf.reset_index()
 thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="DayInWeek", axis=1)

 #day in month
 aMonth=firstInstance.groupby(by=['DayInMonth'])
 #print(thisDf)
 thisMonthDescriptionDf=aMonth.Price.describe()
 thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="count", axis=1)
 thisMonthDescriptionDf=thisMonthDescriptionDf.reset_index()
 thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayInMonth", axis=1)

 # setting index for  months
 a=list(range(31))
 b=[str(a) for a in a]
 month_case_id=["M" + b for b in b]

 #print(thisMonthDescriptionDf)

 if thisMonthDescriptionDf['std'].isnull().values.any():
  thisMonthDescriptionDf['std']=thisMonthDescriptionDf['mean']

 #thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayInMonth", axis=1)

 for dayMissing in range(31-len(thisMonthDescriptionDf)):
  #
  idMissing=dayMissing+len(thisMonthDescriptionDf)
  meanDay=thisMonthDescriptionDf.mean()
  #print(np.transpose(meanDay))
  thisMonthDescriptionDf=thisMonthDescriptionDf.append(meanDay,ignore_index=True)
 #print(thisMonthDescriptionDf)
 thisMonthDescriptionDf.insert(0, "Id_case", month_case_id, True)

 #thisMonthDescriptionDf=thisMonthDescriptionDf.reset_index()
 #thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="index", axis=1)

 #print(thisDayDescriptionDf)
 #print(thisMonthDescriptionDf)
 #print(thisDayDescriptionDf.columns)
 #print(thisMonthDescriptionDf.columns)

 #concatenate
 thisDescriptionDf=pd.concat([thisDayDescriptionDf,thisMonthDescriptionDf], axis=0)
 thisDescriptionDf= thisDescriptionDf.set_index('Id_case')

 thisInstanceDf=thisDescriptionDf.stack(dropna=False).to_frame().T
 thisInstanceDf['Service']=firstInstance['Service']
 thisInstanceDf['ID_Instance']=firstInstance['ID_Instance']
 ds=pd.DataFrame(columns=thisInstanceDf.columns)


 #print(ds)
 for idx,instance in enumerate(df.groupby(by=['ID_Instance'])):
  #day in week
  thisDayDf=instance[1].groupby(by=['DayInWeek'])
  #print(thisDf)
  thisDayDescriptionDf=thisDayDf.Price.describe()
  thisDayDescriptionDf.insert(0, "Id_case", day_case_id, True)
  thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="count", axis=1)
  thisDayDescriptionDf=thisDayDescriptionDf.reset_index()
  thisDayDescriptionDf = thisDayDescriptionDf.drop(labels="DayInWeek", axis=1)
  #thisDayDescriptionDf['Id_case']=list(range(len(thisDayDescriptionDf)))

  #day in month
  thisMonthDf=instance[1].groupby(by=['DayInMonth'])
  #print(len(thisMonthDf))
  thisMonthDescriptionDf=thisMonthDf.Price.describe()
  thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="count", axis=1)
  thisMonthDescriptionDf = thisMonthDescriptionDf.reset_index()
  thisMonthDescriptionDf = thisMonthDescriptionDf.drop(labels="DayInMonth", axis=1)

  #si une seule valeur, pas de std donc on prend la moyenne
  if thisMonthDescriptionDf['std'].isnull().values.any():
   thisMonthDescriptionDf['std']=thisMonthDescriptionDf['mean']

  for dayMissing in range(31-len(thisMonthDescriptionDf)):

   idMissing=dayMissing+len(thisMonthDescriptionDf)
   meanDay=thisMonthDescriptionDf.mean()
   #print(np.transpose(meanDay))
   thisMonthDescriptionDf=thisMonthDescriptionDf.append(meanDay,ignore_index=True)

  thisMonthDescriptionDf.insert(0, "Id_case", month_case_id, True)

  #print(thisDayDescriptionDf.columns)
  #print(thisMonthDescriptionDf.columns)
  #concatenate
  thisDescriptionDf=pd.concat([thisDayDescriptionDf,thisMonthDescriptionDf], axis=0)
  thisDescriptionDf= thisDescriptionDf.set_index('Id_case')

  thisInstanceDf=thisDescriptionDf.stack(dropna=False).to_frame().T
  dayService=thisDayDf.Service.mean().reset_index().Service
  monthService=thisMonthDf.Service.mean().reset_index().Service
  serviceValue=np.mean(pd.concat([dayService,monthService], axis=0))
  thisInstanceDf['Service']=serviceValue

  dayID=thisDayDf.ID_Instance.mean().reset_index().ID_Instance
  monthID=thisMonthDf.ID_Instance.mean().reset_index().ID_Instance
  IDValue=np.mean(pd.concat([dayID,monthID], axis=0))
  thisInstanceDf['ID_Instance']=IDValue

  #print(thisInstanceDf.columns)
  #print(thisInstanceDf)
  ds=ds.append(thisInstanceDf,ignore_index=True)
 #print(ds)

 return ds
# ----------------


# %%s
# Treat the data
# ----------------

def get_X_y(ds):

 # creation des columns
 y = ds['Service'].values

 ds = ds.drop(labels="Service", axis=1)
 ds = ds.drop(labels="ID_Instance", axis=1)
 X = ds.values

 return X , y
# ----------------
